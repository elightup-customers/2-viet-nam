#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Beaver Builder Child Theme\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-18 03:11+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.2.2; wp-5.1.1"

#: inc/Form.php:50
msgid "+ Thêm file"
msgstr ""

#. Description of the theme
msgid ""
"An example child theme that can be used as a starting point for custom "
"development."
msgstr ""

#. Name of the theme
msgid "Beaver Builder Child Theme"
msgstr ""

#: inc/Form.php:44
msgid "Bài dự thi của bạn đã được gửi thành công!"
msgstr ""

#: inc/Vote.php:137
msgid "Bài dự thi không hợp lệ."
msgstr ""

#: inc/Vote.php:80
msgid "Bình chọn"
msgstr ""

#: inc/UserForm.php:30 inc/UserForm.php:52
msgid "Bạn phải đăng nhập để cập nhật thông tin."
msgstr ""

#: inc/Form.php:36
msgid "Bạn phải đăng nhập để nộp bài dự thi."
msgstr ""

#: inc/Vote.php:86
msgid "Bạn đã bình chọn thành công!"
msgstr ""

#: inc/Vote.php:127
msgid "Bạn đã hết lượt bình chọn."
msgstr ""

#: inc/Auth.php:87
msgid ""
"Bạn đã xác nhận qua Facebook thành công. Xin vui lòng đợi hệ thống xử lý."
msgstr ""

#: inc/GoogleRegister.php:27
msgid "Bạn đã xác thực qua Google thành công!"
msgstr ""

#: inc/Auth.php:103
msgid "Bạn đã đăng nhập"
msgstr ""

#: inc/Auth.php:45 inc/Auth.php:63
#, php-format
msgid "Bạn đã đăng nhập qua Facebook với tên <strong>%s</strong>."
msgstr ""

#: inc/Auth.php:95
#, php-format
msgid "Bạn đã đăng nhập thành công qua Facebook với tên <strong>%s</strong>."
msgstr ""

#: inc/Auth.php:53
msgid "Bạn đã đăng xuất thành công."
msgstr ""

#: inc/Form.php:54
msgid "Chia sẻ trải nghiệm của bạn (không quá 500 từ)"
msgstr ""

#: inc/helpers.php:104
msgid "Có lỗi xảy ra. Hãy thông báo cho quản trị viên biết để khắc phục."
msgstr ""

#: inc/helpers.php:106
msgid "Có lỗi xảy ra: "
msgstr ""

#: inc/UserForm.php:37
msgid "Cập nhật"
msgstr ""

#: inc/UserForm.php:66
msgid "Danh sách bài dự thi đã nộp"
msgstr ""

#: inc/Form.php:43
msgid "Gửi bài"
msgstr ""

#. Author URI of the theme
msgid "http://www.fastlinemedia.com"
msgstr ""

#. URI of the theme
msgid "http://www.wpbeaverbuilder.com"
msgstr ""

#. Name of the template
msgid "Long form"
msgstr ""

#: inc/Vote.php:67
msgid "lượt bình chọn"
msgstr ""

#: inc/Form.php:31 inc/UserForm.php:25 inc/UserForm.php:47
msgid "Lỗi không xác thực được người dùng."
msgstr ""

#: inc/Form.php:79 inc/Form.php:85 inc/Vote.php:109
msgid "Lỗi không xác thực được người dùng. Vui lòng liên hệ với quản trị viên."
msgstr ""

#: inc/Form.php:203
msgid "Nộp bài thành công"
msgstr ""

#. Author of the theme
msgid "The Beaver Builder Team"
msgstr ""

#: inc/UserForm.php:38
msgid "Thông tin của bạn đã được cập nhật thành công!"
msgstr ""

#: inc/helpers.php:114
msgid "Trở về trang chủ"
msgstr ""

#: inc/helpers.php:117
msgid "Trở về trang trước"
msgstr ""

#: inc/Form.php:66
msgid "Tên tác phẩm dự thi"
msgstr ""

#: inc/Form.php:41
msgid ""
"Vui lòng điền đầy đủ các thông tin sau để nộp bài dự thi. Mọi thắc mắc vui "
"lòng liên hệ hỗ trợ tại hotline của chúng tôi. Lưu ý, các mục đánh dấu sao "
"(*) là bắt buộc phải điền."
msgstr ""

#: inc/UserForm.php:35
msgid ""
"Vui lòng điền đầy đủ các thông tin sau. Mọi thắc mắc vui lòng liên hệ hỗ trợ "
"tại hotline của chúng tôi. Lưu ý, các mục đánh dấu sao (*) là bắt buộc phải "
"điền."
msgstr ""

#: inc/Vote.php:115
msgid "Vui lòng đăng nhập để bình chọn."
msgstr ""

#: inc/Auth.php:203
#, php-format
msgid "Xin chào! <strong><a href=\"%s\">%s</a></strong>"
msgstr ""

#: inc/Auth.php:199
msgid "Đăng kí"
msgstr ""

#: inc/Auth.php:105
msgid "Đăng ký bằng Facebook"
msgstr ""

#: inc/Auth.php:198
msgid "Đăng nhập"
msgstr ""

#: inc/Auth.php:50
msgid "Đăng nhập bằng Facebook"
msgstr ""

#: inc/Auth.php:91
msgid ""
"Đăng nhập không thành công. Không thể tạo tài khoản cho bạn. Vui lòng liên "
"hệ quản trị viên để được trợ giúp."
msgstr ""

#: inc/Auth.php:47 inc/Auth.php:203
msgid "Đăng xuất"
msgstr ""
