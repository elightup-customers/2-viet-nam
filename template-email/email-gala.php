<table cellpadding="0" cellspacing="0" border="0" width="700" align="center">
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-top.jpg" width="700" height="8" style="display: block" alt="">
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-left.jpg" width="8" height="1050" style="display: block" alt="">
                    </td>
                    <td align="center" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="684">
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/logo.jpg" width="145" height="99" style="display: block" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/banner-new1.jpg" width="601" height="401" style="display: block" alt="">
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <table cellspacing="0" cellpadding="0" border="0" width="584">
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b;" align="left" valign="top">
                                                2! Vietnam xin chào <?php echo $name; ?>!
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="8" style="font-size: 0">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                Cảm ơn bạn đã quan tâm đến chương trình 2! Vietnam và tham dự cuộc thi "2! Vietnam photography and video contest". Sau khi chấm điểm và tổng kết các bài dự thi lọt vào vòng Chung kết, tác phẩm '<?php echo $post_title; ?>' của bạn được Hội đồng giám khảo đánh giá là một trong những tác phẩm xuất sắc nhất và có cơ hội giành giải thưởng cao nhất của cuộc thi.

                                                <br><br>
                                                2! Vietnam trân trọng kính mời bạn:<br>
                                                Tới tham dự: "Đêm Gala trao giải cuộc thi "2! Vietnam photography and video contest"<br>
                                                Thời gian:<br>
                                                Địa điểm:<br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="middle">
                                    <table cellspacing="0" cellpadding="0" border="0" width="584">
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                <br>
                                              Sự có mặt của bạn sẽ góp phần to lớn vào thành công chung của chương trình. Bạn vui lòng xác nhận sự tham gia bằng cách phản hồi email này. Ban tổ chức sẽ liên hệ trực tiếp với bạn để hướng dẫn chi tiết cách thức tham dự chương trình. Xin nhiệt liệt chúc mừng bạn!<br><br>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                Mời bạn hãy tiếp tục đồng hành cùng 2! Vietnam trong các hoạt động tiếp theo nhé.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                <br>
                                                Trân trọng!
                                                <br>
                                                2! Vietnam.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </td>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-right.jpg" width="8" height="1050" style="display: block" alt="">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-bot.jpg" width="700" height="47" style="display: block" alt="">
        </td>
    </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" border="0" width="700" align="center">
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-top.jpg" width="700" height="8" style="display: block" alt="">
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-left.jpg" width="8" height="1050" style="display: block" alt="">
                    </td>
                    <td align="center" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="684">
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/logo.jpg" width="145" height="99" style="display: block" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/banner-new1.jpg" width="601" height="401" style="display: block" alt="">
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <table cellspacing="0" cellpadding="0" border="0" width="584">
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b;" align="left" valign="top">
                                                2! Vietnam's warmest greetings to <?php echo $name; ?>!
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="8" style="font-size: 0">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                Thank you for being interested in 2! Vietnam program and enter "2! Vietnam photography and video contest". After evaluating all the entries in the month of ..., your entry '<?php echo $post_title; ?>' has been selected by our Professional Council as one of the best quality entries for our highest prize.

                                                <br><br>
                                                2! Vietnam would like to invite you:<br>
                                                To participate: Gala night award for "2! Vietnam photography and video contest"<br>
                                                Time and date:<br>
                                                Location:<br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="middle">
                                    <table cellspacing="0" cellpadding="0" border="0" width="584">
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                <br>
                                               We genuinely hope that you honor us with your presence. Should you accept our invitation, please confirm with us by responding to this email. Our organizer will contact you directly to guide you how to enter our event. Congratulations!<br><br>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                We hope to accompany you in our nearest activities.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                <br>
                                                Best regards!
                                                <br>
                                                2! Vietnam.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </td>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-right.jpg" width="8" height="1050" style="display: block" alt="">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-bot.jpg" width="700" height="47" style="display: block" alt="">
        </td>
    </tr>
</table>