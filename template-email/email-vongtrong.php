<table cellpadding="0" cellspacing="0" border="0" width="700" align="center">
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-top.jpg" width="700" height="8" style="display: block" alt="">
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-left.jpg" width="8" height="1050" style="display: block" alt="">
                    </td>
                    <td align="center" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="684">
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/logo.jpg" width="145" height="99" style="display: block" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/banner-new1.jpg" width="601" height="401" style="display: block" alt="">
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <table cellspacing="0" cellpadding="0" border="0" width="584">
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b;" align="left" valign="top">
                                                2! Vietnam xin chào <?php echo $name; ?>!
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="8" style="font-size: 0">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                Ban tổ chức cuộc thi "2! Vietnam photography and video contest" trận trọng thông báo: <br>
                                                Tác phẩm '<?php echo $post_title; ?>' của bạn đã được Hội đồng giám khảo lựa chọn vào vòng Chung kết của cuộc thi "2! Vietnam". Tác phẩm của bạn cùng các tác phẩm dự thi xuất sắc khác sẽ được trưng bày trong triển lãm "2! Vietnam's Best Collection", diễn ra vào 2 ngày 5-6/10/2019 tại Phố đi bộ Hồ Gươm. Xin nhiệt liệt chúc mừng bạn!<br><br>
                                                Bạn có thể xem các tác phẩm lọt vào vòng Chung kết bằng cách click nút dưới đây
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="middle">
                                    <table cellspacing="0" cellpadding="0" border="0" width="186">
                                        <tr>
                                            <td width="186" height="50" align="left" valign="top">
                                                <a href="https://demo.2vietnam.vn/vong-thi/vong-chung-ket/" style="display: block;font-family: Arial;font-size: 20px;color: #a71e23;font-weight: bold; text-align: center;text-decoration: none;line-height: 50px;background-color: #f5c81e">
                                                    To the Collection
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="middle">
                                    <table cellspacing="0" cellpadding="0" border="0" width="584">
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                <br>
                                               Ban tổ chức sẽ liên tục cập nhật thông tin về những hoạt động thú vị của vòng chung kết "2! Vietnam photography and video contest" trong các email tiếp theo. Bạn hãy thường xuyên theo dõi nhé
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="24" style="font-size: 0">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                <br>
                                                Trân trọng!
                                                <br>
                                                2! Vietnam.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </td>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-right.jpg" width="8" height="1050" style="display: block" alt="">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-bot.jpg" width="700" height="47" style="display: block" alt="">
        </td>
    </tr>
</table>
<br>

<table cellpadding="0" cellspacing="0" border="0" width="700" align="center">
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-top.jpg" width="700" height="8" style="display: block" alt="">
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-left.jpg" width="8" height="1050" style="display: block" alt="">
                    </td>
                    <td align="center" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="684">
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/logo.jpg" width="145" height="99" style="display: block" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/banner-new1.jpg" width="601" height="401" style="display: block" alt="">
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <table cellspacing="0" cellpadding="0" border="0" width="584">
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b;" align="left" valign="top">
                                                2! Vietnam's warmest greetings to <?php echo $name; ?>!
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="8" style="font-size: 0">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                2! Vietnam is very happy to inform you that: <br>
                                                Your entry '<?php echo $post_title; ?>' has been selected by our Professional Council as one of the best works to reach the Finals of "2! Vietnam photography and video contest". Your entry along with all the entries in the Finals will be displayed in our Exhibition "2! Vietnam's Best Collection" on 5th and 65h October 2019 at Ho Guom street. Congratulations!<br><br>
                                                Your can watch all the entries the reach the Finals by clicking the button below
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="middle">
                                    <table cellspacing="0" cellpadding="0" border="0" width="186">
                                        <tr>
                                            <td width="186" height="50" align="left" valign="top">
                                                <a href="https://demo.2vietnam.vn/vong-thi/vong-chung-ket/" style="display: block;font-family: Arial;font-size: 20px;color: #a71e23;font-weight: bold; text-align: center;text-decoration: none;line-height: 50px;background-color: #f5c81e">
                                                    To the Collection
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="middle">
                                    <table cellspacing="0" cellpadding="0" border="0" width="584">
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                <br>
                                               We will update you all the infomation about many interesting activities of the Finals in the next emails. Please follow us regularly.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="24" style="font-size: 0">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                <br>
                                                Best regards!
                                                <br>
                                                2! Vietnam.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </td>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-right.jpg" width="8" height="1050" style="display: block" alt="">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-bot.jpg" width="700" height="47" style="display: block" alt="">
        </td>
    </tr>
</table>