<table cellpadding="0" cellspacing="0" border="0" width="700" align="center">
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-top.jpg" width="700" height="8" style="display: block" alt="">
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-left.jpg" width="8" height="988" style="display: block" alt="">
                    </td>
                    <td align="center" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="684">
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/logo.jpg" width="145" height="99" style="display: block" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/banner-new1.jpg" width="601" height="401" style="display: block" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table cellspacing="0" cellpadding="0" border="0" width="584">
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b;" align="left" valign="top">
                                                2! Vietnam xin chào <?php echo $name; ?>!
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="8" style="font-size: 0">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                Cảm ơn bạn đã quan tâm đến Chương trình 2! Vietnam và cuộc thi ảnh/video của chúng tôi. <br>
                                                Vui lòng click vào link dưới đây để được xác thực đăng ký: <br>
                                                <a href="<?php echo $link_confirm; ?>"><?php echo $link_confirm; ?></a><br>
                                                Sau khi xác thực thành công, bạn sẽ đủ điều kiện tham gia cuộc thi "2! Vietnam photography and video contest"
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="8" style="font-size: 0">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                Bạn muốn tham gia cuộc thi, vui lòng truy cập <a href="https://demo.2vietnam.vn/nop-bai-du-thi/">https://demo.2vietnam.vn/nop-bai-du-thi/.</a> Bạn muốn hòa mình vào 2! Vietnam để chia sẻ tình yêu Việt Nam, lan tỏa tình yêu ấy với cộng đồng, vui lòng truy cập website để theo dõi các hoạt động hấp dẫn của chương trình. Chúng tôi sẽ cập nhật thường xuyên: <a href="https://demo.2vietnam.vn/">https://demo.2vietnam.vn/</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                <br>
                                                Trân trọng!
                                                <br>
                                                2! Vietnam.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-right.jpg" width="8" height="988" style="display: block" alt="">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-bot.jpg" width="700" height="47" style="display: block" alt="">
        </td>
    </tr>
</table>
