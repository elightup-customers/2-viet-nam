<table cellpadding="0" cellspacing="0" border="0" width="700" align="center">
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-top.jpg" width="700" height="8" style="display: block" alt="">
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-left.jpg" width="8" height="988" style="display: block" alt="">
                    </td>
                    <td align="center" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="684">
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/logo.jpg" width="145" height="99" style="display: block" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/banner-new1.jpg" width="601" height="401" style="display: block" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table cellspacing="0" cellpadding="0" border="0" width="584">
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b;" align="left" valign="top">
                                                2! Vietnam xin chào <?php echo $name; ?>!
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="8" style="font-size: 0">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                Cảm ơn bạn đã quan tâm và tham gia cuộc thi "2! Vietnam photography and video contest".<br><br>
                                                Ban tổ chức cuộc thi rất tiếc phải thông báo: Tác phẩm '<?php echo $post_title; ?>' của bạn chưa đủ điều kiện dự thi.<br>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                <br>
                                                Lý do:... 2! Vietnam hi vọng bạn sẽ khắc phục lý do trên và gửi lại bài dự thi hoặc tiếp tục tham gia cuộc thi bằng các tác phẩm mới.
                                                <br><br>
                                                <a href="https://demo.2vietnam.vn/the-le-cuoc-thi/">Mời bạn theo dõi thông tin thể lệ cuộc thi tại file đính kèm</a> và hãy tiếp tục đồng hành cùng 2! Vietnam trong các hoạt động tiếp theo nhé.
                                                <br><br>
                                                Trân trọng!
                                                <br>
                                                2! Vietnam.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td height="34" style="font-size: 0">&nbsp;</td>
                            </tr>

                        </table>
                    </td>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-right.jpg" width="8" height="988" style="display: block" alt="">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-bot.jpg" width="700" height="47" style="display: block" alt="">
        </td>
    </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" border="0" width="700" align="center">
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-top.jpg" width="700" height="8" style="display: block" alt="">
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-left.jpg" width="8" height="988" style="display: block" alt="">
                    </td>
                    <td align="center" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="684">
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/logo.jpg" width="145" height="99" style="display: block" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size: 0">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/banner-new1.jpg" width="601" height="401" style="display: block" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table cellspacing="0" cellpadding="0" border="0" width="584">
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b;" align="left" valign="top">
                                                2! Vietnam's warmest greetings to <?php echo $name; ?>!
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="8" style="font-size: 0">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                Thank you for interested in and joing our "2! Vietnam photography and video contest".<br><br>
                                                We are very sorry to inform you that: Your entry '<?php echo $post_title; ?>' is not qualified to enter the contest. <br>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial;font-size: 17px;color: #0b0b0b; line-height: 21px;text-align: justify;" align="left" valign="top">
                                                <br>
                                                The reason is: .... 2! Vietnam hope that you will overcome these reasons and resend or continue joining our contest with other entries.
                                                <br><br>
                                                <a href="https://demo.2vietnam.vn/en/entry-guidelines/">Please follow our rules in the attached document below</a> and we hope you will join us in 2! Vietnam's many acitives to come in future.
                                                <br><br>
                                                Best regards!
                                                <br>
                                                2! Vietnam.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td height="34" style="font-size: 0">&nbsp;</td>
                            </tr>

                        </table>
                    </td>
                    <td align="center" style="font-size: 0" width="8">
                        <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-right.jpg" width="8" height="988" style="display: block" alt="">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-size: 0">
            <img src="https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/template-email/images/line-bot.jpg" width="700" height="47" style="display: block" alt="">
        </td>
    </tr>
</table>