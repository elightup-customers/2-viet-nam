<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

require 'vendor/autoload.php';

// Classes
require_once 'inc/widget/class-recent-posts-widget.php';
require_once 'inc/widget/class-trending-widget.php';
require_once 'inc/class-fl-child-theme.php';
require_once 'inc/UI.php';
require_once 'inc/helpers.php';
require_once 'inc/Auth.php';
require_once 'inc/Form.php';
require_once 'inc/UserForm.php';
require_once 'inc/EmailRegister.php';
require_once 'inc/GoogleRegister.php';
require_once 'inc/Image.php';
require_once 'inc/Vote.php';

new UI();
$hivn_auth = new Auth();
new Form( $hivn_auth );
new UserForm( $hivn_auth );
new EmailRegister( $hivn_auth );
new GoogleRegister( $hivn_auth );
new Vote( $hivn_auth );


/*
 * Login redirect.
 */
function login_redirect( $redirect_to, $request, $user ){
    return home_url();
}
add_filter( 'login_redirect', 'login_redirect', 10, 3 );

/*
 * Add custom class for archive bai-du-thi ver T.A
 */
function hi2n_body_classes( $classes ){
	if ( is_post_type_archive( 'bai-du-thi' ) ) {
		if ( function_exists( 'pll_current_language' ) ) {
			if ( 'English' === pll_current_language( 'name' ) ) {
				$classes[] = 'archive-bai-du-thi-en';
			}
		}
	}
	return $classes;
}
add_filter( 'body_class', 'hi2n_body_classes' );

// Load translation files from your child theme instead of the parent theme
function my_child_theme_locale() {
	load_child_theme_textdomain( '2vn', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'my_child_theme_locale' );

add_action( 'init', function() use ( $hivn_auth ) {
	add_shortcode( 'btn_tham_gia', function( $atts ) use ( $hivn_auth ) {
		$a = shortcode_atts( array(
			'title' => __( 'Đăng kí', '2vn' ),
			'icon'  => '',
			'style' => '',
			'class' => '',
		), $atts );
		if ( $hivn_auth->is_logged_in() ) {
			if ( 'English' === pll_current_language( 'name' ) ) {
				$content = '<a href="/en/entry-enquiry/" class="' . $a['class'] . '" style="' . $a['style'] . '">' . $a['title'] . wp_kses_post( $a['icon'] ) . '</a>';
				return $content;
			} else {
				$content = '<a href="/nop-bai-du-thi/" class="' . $a['class'] . '" style="' . $a['style'] . '">' . $a['title'] . wp_kses_post( $a['icon'] ) . '</a>';
				return $content;
			}
		} else {
			if ( 'English' === pll_current_language( 'name' ) ) {
				$content = '<a href="/en/register/" class="' . $a['class'] . '" style="' . $a['style'] . '">' . $a['title'] . wp_kses_post( $a['icon'] ) . '</a>';
				return $content;
			} else {
				$content = '<a href="/dang-ky-2/" class="' . $a['class'] . '" style="' . $a['style'] . '">' . $a['title'] . wp_kses_post( $a['icon'] ) . '</a>';
				return $content;
			}
		}
	} );
} );


// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

/*
 * Custom fonts
 */
function my_bb_custom_fonts( $system_fonts ) {
	$system_fonts['SanFranciscoDisplay'] = array(
		'fallback' => 'sans-serif',
		'weights'  => array(
			'400',
			'500',
			'700',
		),
	);
	$system_fonts['SH Imogen Agnes']     = array(
		'fallback' => 'sans-serif',
		'weights'  => array(
			'400',
		),
	);
	$system_fonts['HawaiiLover']     = array(
		'fallback' => 'sans-serif',
		'weights'  => array(
			'400',
		),
	);
	$system_fonts['Getsu Magic']     = array(
		'fallback' => 'sans-serif',
		'weights'  => array(
			'400',
		),
	);
	return $system_fonts;
}
// Add to Beaver Builder Theme Customizer.
add_filter( 'fl_theme_system_fonts', 'my_bb_custom_fonts' );
// Add to Page Builder modules.
add_filter( 'fl_builder_font_families_system', 'my_bb_custom_fonts' );



/*
 * Add Child theme support
 */
function flchild_setup() {
	add_image_size( 'related-image', 237, 140, true );
	add_image_size( 'section-video-image', 685, 437, true );
	add_image_size( 'image-1', 570, 570, true );
	add_image_size( 'image-2', 570, 270, true );
}
add_action( 'after_setup_theme', 'flchild_setup' );

/*
 * register widget
 */
function fl_widgets_init() {
	register_widget( 'FLChildTheme_Recent_Posts_Widget' );
	register_widget( 'FLChildTheme_Trending_Widget' );
}
add_action( 'widgets_init', 'fl_widgets_init' );

/**
 * Prints HTML with meta information for the current post-date/time.
 */
function flchild_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf(
		$time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	echo '<span class="posted-on">' . $time_string . '</span>'; // WPCS: XSS OK.
}

/**
 * Excerpt Length.
 */
function flchild_excerpt_length( $length ) {
	return 21;
}
add_filter( 'excerpt_length', 'flchild_excerpt_length' );



/**
 * Filter bình chọn bài dự thi archive
 */
function custom_filter_baiduthi( $query ) {
	if ( ( is_post_type_archive( 'bai-du-thi' ) ) && $query->is_main_query() && ! is_admin() ) {
		$query->set( 'posts_per_page', 9999 );
		$query->set( 'lang', 'vi' );
		if ( isset( $_GET['orderby'] ) ) {
			if ( ( 'votes' === $_GET['orderby'] ) && ( 'DESC' === $_GET['order'] ) ) {
				$query->set( 'orderby', 'meta_value' );
				$query->set( 'meta_key', 'votes' );
				$query->set( 'order', 'DESC' );
			}
			if ( 'rand' === $_GET['orderby'] ) {
				$query->set( 'orderby', 'rand' );
			}
			if ( 'date' === $_GET['orderby'] ) {
				$query->set( 'orderby', 'date' );
			}
		}

		if ( ! empty( $_GET['getby'] ) ) {
			if ( 'cat' === $_GET['getby'] ) {
				$taxquery = array(
					array(
						'taxonomy' => 'vong-thi',
						'field' => 'slug',
						'terms' => $_GET['cat'],
					),
				);
				$query->set( 'tax_query', $taxquery );
			}
		}
	}

	if ( is_page( 'binh-chon-bai-du-thi-webview' ) || is_page( 'binh-chon-bai-du-thi-webview-2' ) ) {
		if ( isset( $_GET['orderby'] ) ) {
			if ( ( 'votes' === $_GET['orderby'] ) && ( 'DESC' === $_GET['order'] ) ) {
				$query->set( 'orderby', 'meta_value' );
				$query->set( 'meta_key', 'votes' );
				$query->set( 'order', 'DESC' );
			}
			if ( 'rand' === $_GET['orderby'] ) {
				$query->set( 'orderby', 'rand' );
			}
			if ( 'date' === $_GET['orderby'] ) {
				$query->set( 'orderby', 'date' );
			}
		}

		if ( ! empty( $_GET['getby'] ) ) {
			if ( 'cat' === $_GET['getby'] ) {
				$taxquery = array(
					array(
						'taxonomy' => 'vong-thi',
						'field' => 'id',
						'terms' => $_GET['cat'],
					),
				);
				$query->set( 'tax_query', $taxquery );
			}
		}
		$query->set( 'posts_per_page', 9999 );
	}

	return $query;
}
add_action( 'pre_get_posts', 'custom_filter_baiduthi' );

/**
 * Filter bình chọn bài dự thi webview tiếng anh
 */
function custom_query_webview( $query_args ) {
	if ( 'binhchon-webview-english' == $query_args['settings']->id ) {
		$query_args['lang'] = 'vi';
	}
	return $query_args;
}
add_filter( 'fl_builder_loop_query_args', 'custom_query_webview' );

/**
 *
 *
 * @return fb comments.
 */
function flchild_comments() {
	?>
	<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%"></div>
	<?php
}


function shortcode_fb_comments() {
	ob_start();
	echo flchild_comments();
	return ob_get_clean();
}
add_shortcode( 'comments', 'shortcode_fb_comments' );

/**
 * Chèn JS của FB vào hook footer
 */
function load_js_fb(){
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			?>
			<!-- Load Facebook SDK for JavaScriptt -->
			<div id="fb-root"></div>
			<script>
			  window.fbAsyncInit = function() {
			    FB.init({
			      xfbml            : true,
			      version          : 'v3.3'
			    });
			  };

			  (function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<?php
		} else {
			?>
			<!-- Load Facebook SDK for JavaScriptt -->
			<div id="fb-root"></div>
			<script>
			  window.fbAsyncInit = function() {
			    FB.init({
			      xfbml            : true,
			      version          : 'v3.3'
			    });
			  };

			  (function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<?php
		}
	}

}
add_action( 'wp_footer', 'load_js_fb' );

/*
 * Get ID Youtube video
 */
function get_youtube_video_ID( $youtube_video_url ) {
	/**
	* Pattern matches
	* http://youtu.be/ID
	* http://www.youtube.com/embed/ID
	* http://www.youtube.com/watch?v=ID
	* http://www.youtube.com/?v=ID
	* http://www.youtube.com/v/ID
	* http://www.youtube.com/e/ID
	* http://www.youtube.com/user/username#p/u/11/ID
	* http://www.youtube.com/leogopal#p/c/playlistID/0/ID
	* http://www.youtube.com/watch?feature=player_embedded&v=ID
	* http://www.youtube.com/?feature=player_embedded&v=ID
	*/
	$pattern =
		'%
		(?:youtube                    # Match any youtube url www or no www , https or no https
		(?:-nocookie)?\.com/          # allows for the nocookie version too.
		(?:[^/]+/.+/                  # Once we have that, find the slashes
		|(?:v|e(?:mbed)?)/|.*[?&]v=)  # Check if its a video or if embed
		|youtu\.be/)                  # Allow short URLs
		([^"&?/ ]{11})                # Once its found check that its 11 chars.
		%i';
	// Checks if it matches a pattern and returns the value.
	if ( preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $youtube_video_url, $match ) ) {
		return $match[1];
	}

	// if no match return false.
	return false;
}


/*
 * Add shortcode
 */
function shortcode_baiduthi_thumbnail() {
	ob_start();
	$bdt_id         = get_the_ID();
	$image_object   = new Image();
	$the_loai       = rwmb_meta( 'type' );
	$images         = $image_object->get_images( $bdt_id );
	$video_link     = rwmb_get_value( 'video' );
	// $cut_video_link = substr( $video_link, 32 );
	$cut_video_link = get_youtube_video_ID( $video_link );

	if ( 'Hình ảnh' === $the_loai->name ) {
		echo '<a href="' . esc_url( get_the_permalink() ) . '"><img src="' . esc_url( $images[0]['large'] ) . '"/></a>';
	} else {
		echo '<a href="' . esc_url( get_the_permalink() ) . '"><img src="https://i.ytimg.com/vi/' . esc_html( $cut_video_link ) . '/maxresdefault.jpg"/></a>';
	}
	return ob_get_clean();
}
add_shortcode( 'thumbnail', 'shortcode_baiduthi_thumbnail' );

function shortcode_icon() {
	ob_start();
	$bdt_id   = get_the_ID();
	$the_loai = rwmb_meta( 'type' );
	if ( 'Hình ảnh' === $the_loai->name ) {
		echo '<i class="fas fa-camera-retro"></i>';
	} else {
		echo '<i class="fas fa-video"></i>';
	}
	return ob_get_clean();
}
add_shortcode( 'icon', 'shortcode_icon' );

function shortcode_thumbnail_single_duthi() {
	ob_start();
	$bdt_id         = get_the_ID();
	$the_loai       = rwmb_meta( 'type' );
	$video          = rwmb_meta( 'video' );
	// $gallery_images = get_post_meta( $bdt_id, 'images' );
	$image_object = new Image();
	$images = $image_object->get_images( $bdt_id );

	if ( 'Hình ảnh' === $the_loai->name ) {
		echo '<div class="gallery-thumbnail-wrapper"><div class="gallery-thumbnail">';
		foreach ( $images as $image ) {
			echo '<img src="' . esc_url( $image['large'] ) . '"/>';
		}
		echo '</div>';

		echo '<div class="slider-gallery-thumbnail">';
		foreach ( $images as $image ) {
			echo '<img src="' . esc_url( $image['small'] ) . '"/>';
		}
		echo '</div>';
	} else {
		echo $video;
	}
	return ob_get_clean();
}
add_shortcode( 'thumbnail-single-duthi', 'shortcode_thumbnail_single_duthi' );

function shortcode_display_user() {
	ob_start();

	$user_id = rwmb_meta( 'user' );
	$user    = get_userdata( $user_id );
	echo esc_html( $user->first_name );

	return ob_get_clean();
}
add_shortcode( 'display_user', 'shortcode_display_user' );

function shortcode_vongthi() {
	ob_start();

	$term_args = array(
		'orderby'       => 'term_id',
		'order'         => 'ASC',
		'hide_empty'    => true,
		'parent'        => '8',
	);
	$terms = get_terms( 'vong-thi', $term_args );
	$term_list = '<p class="term-vongsoloai">';
	$uri = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$num_terms = count( $terms );
	$i = 0;
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			$name_arr = array(
				'April' => esc_html__( 'Tháng 4', '2vn' ),
				'May' => esc_html__( 'Tháng 5', '2vn' ),
				'June' => esc_html__( 'Tháng 6', '2vn' ),
				'July' => esc_html__( 'Tháng 7', '2vn' ),
				'August' => esc_html__( 'Tháng 8', '2vn' ),
				'September' => esc_html__( 'Tháng 9', '2vn' ),
				'October' => esc_html__( 'Tháng 10', '2vn' ),
				'November' => esc_html__( 'Tháng 11', '2vn' ),
				'December' => esc_html__( 'Tháng 12', '2vn' ),
			);
		} else {
			$name_arr = array(
				'Tháng 4' => esc_html__( 'Tháng 4', '2vn' ),
				'Tháng 5' => esc_html__( 'Tháng 5', '2vn' ),
				'Tháng 6' => esc_html__( 'Tháng 6', '2vn' ),
				'Tháng 7' => esc_html__( 'Tháng 7', '2vn' ),
				'Tháng 8' => esc_html__( 'Tháng 8', '2vn' ),
				'Tháng 9' => esc_html__( 'Tháng 9', '2vn' ),
				'Tháng 10' => esc_html__( 'Tháng 10', '2vn' ),
				'Tháng 11' => esc_html__( 'Tháng 11', '2vn' ),
				'Tháng 12' => esc_html__( 'Tháng 12', '2vn' ),
			);
		}
	}

	foreach ( $terms as $term ) {
		foreach ( $name_arr as $key => $value ) {
			if ( $value == $term->name ) {
				$name = $key;
			}
		}

		$class = '';
		if ( $uri == get_term_link( $term ) ) {
			$class = 'current';
		}
		$term_list .= '<a class="' . esc_html( $class ) . '" href="?getby=cat&cat=' . esc_html( $term->slug ) . '">' . $name . '</a>';
		if ( ++$i !== $num_terms ) {
			$term_list .= '|';
		}
	}
	echo $term_list;

	return ob_get_clean();
}
add_shortcode( 'ds_vongthi', 'shortcode_vongthi' );


function shortcode_vongthi_webview() {
	ob_start();

	$term_args = array(
		'orderby'       => 'term_id',
		'order'         => 'ASC',
		'hide_empty'    => true,
		'parent'        => '8',
	);
	$terms = get_terms( 'vong-thi', $term_args );
	$term_list = '<p class="term-vongsoloai">';
	$uri = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$num_terms = count( $terms );
	$i = 0;
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			$name_arr = array(
				'April' => esc_html__( 'Tháng 4', '2vn' ),
				'May' => esc_html__( 'Tháng 5', '2vn' ),
				'June' => esc_html__( 'Tháng 6', '2vn' ),
				'July' => esc_html__( 'Tháng 7', '2vn' ),
				'August' => esc_html__( 'Tháng 8', '2vn' ),
				'September' => esc_html__( 'Tháng 9', '2vn' ),
				'October' => esc_html__( 'Tháng 10', '2vn' ),
				'November' => esc_html__( 'Tháng 11', '2vn' ),
				'December' => esc_html__( 'Tháng 12', '2vn' ),
			);
		} else {
			$name_arr = array(
				'Tháng 4' => esc_html__( 'Tháng 4', '2vn' ),
				'Tháng 5' => esc_html__( 'Tháng 5', '2vn' ),
				'Tháng 6' => esc_html__( 'Tháng 6', '2vn' ),
				'Tháng 7' => esc_html__( 'Tháng 7', '2vn' ),
				'Tháng 8' => esc_html__( 'Tháng 8', '2vn' ),
				'Tháng 9' => esc_html__( 'Tháng 9', '2vn' ),
				'Tháng 10' => esc_html__( 'Tháng 10', '2vn' ),
				'Tháng 11' => esc_html__( 'Tháng 11', '2vn' ),
				'Tháng 12' => esc_html__( 'Tháng 12', '2vn' ),
			);
		}
	}

	foreach ( $terms as $term ) {
		foreach ( $name_arr as $key => $value ) {
			if ( $value == $term->name ) {
				$name     = $key;
			}
		}

		$class = '';
		if ( $uri == get_term_link( $term ) ) {
			$class = 'current';
		}
		$term_list .= '<a class="' . esc_html( $class ) . '" href="?getby=cat&cat=' . esc_html( $term->term_id ) . '">' . $name . '</a>';
		if ( ++$i !== $num_terms ) {
			$term_list .= '|';
		}
	}
	echo $term_list;

	return ob_get_clean();
}
add_shortcode( 'ds_vongthi_webview', 'shortcode_vongthi_webview' );


function shortcode_soloai_chungket() {
	ob_start();

	$term_args = array(
		'orderby'       => 'term_id',
		'order'         => 'ASC',
		'hide_empty'    => true,
		'parent'        => 0,
	);
	$terms = get_terms( 'vong-thi', $term_args );
	$term_list = '';
	$uri = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			$name_arr = array(
				'Preliminary round' => esc_html__( 'Vòng sơ loại', '2vn' ),
				'Final round' => esc_html__( 'Vòng chung kết', '2vn' ),
			);
		} else {
			$name_arr = array(
				'Vòng sơ loại' => esc_html__( 'Vòng sơ loại', '2vn' ),
				'Vòng chung kết' => esc_html__( 'Vòng chung kết', '2vn' ),
			);
		}
	}

	foreach ( $terms as $term ) {
		foreach ( $name_arr as $key => $value ) {
			if ( $value == $term->name ) {
				$name = $key;
			}
		}
		$class = '';
		$class .= $term->slug;
		if ( $uri == get_term_link( $term ) ) {
			$class .= ' current';
		}
		$term_list .= '<a class="btn ' . esc_html( $class ) . '" href="' . esc_url( get_term_link( $term ) ) . '">' . $name . '</a>';

		$term_list .= '<a class="btn' . esc_html( $class ) . '" href="?getby=cat&cat=' . esc_html( $term->slug ) . '">' . $name . '</a>';
	}
	echo $term_list;

	return ob_get_clean();
}
add_shortcode( 'ds_soloai_chungket', 'shortcode_soloai_chungket' );


function shortcode_soloai_chungket_webview() {
	ob_start();

	$term_args = array(
		'orderby'       => 'term_id',
		'order'         => 'ASC',
		'hide_empty'    => true,
		'parent'        => 0,
	);
	$terms = get_terms( 'vong-thi', $term_args );
	$term_list = '';
	$uri = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			$name_arr = array(
				'Preliminary round' => esc_html__( 'Vòng sơ loại', '2vn' ),
				'Final round' => esc_html__( 'Vòng chung kết', '2vn' ),
			);
		} else {
			$name_arr = array(
				'Vòng sơ loại' => esc_html__( 'Vòng sơ loại', '2vn' ),
				'Vòng chung kết' => esc_html__( 'Vòng chung kết', '2vn' ),
			);
		}
	}

	foreach ( $terms as $term ) {
		foreach ( $name_arr as $key => $value ) {
			if ( $value == $term->name ) {
				$name = $key;
			}
		}

		$class = '';
		$class .= $term->slug;
		if ( $uri == get_term_link( $term ) ) {
			$class .= ' current';
		}
		$term_list .= '<a class="btn ' . esc_html( $class ) . '" href="?getby=cat&cat=' . esc_html( $term->term_id ) . '">' . $name . '</a>';
	}
	echo $term_list;

	return ob_get_clean();
}
add_shortcode( 'ds_soloai_chungket_webview', 'shortcode_soloai_chungket_webview' );


function shortcode_count_baiduthi() {
	ob_start();

	$arg = array(
		'post_type' => 'bai-du-thi',
		'lang'      => 'vi',
	);
	$query = new WP_Query( $arg );
	$total = $query->found_posts;
	echo wp_kses_post( '<span class="number" data-count="' . esc_html( $total ) . '">' . esc_html( $total ) . '</span>' );

	return ob_get_clean();
}
add_shortcode( 'count_baiduthi', 'shortcode_count_baiduthi' );

function shortcode_count_total_votes() {
	ob_start();
	global $wpdb;
	$sql = "select sum(meta_value) as total from wp_postmeta where meta_key='votes'";
	$result = $wpdb->get_results( $sql );
	echo wp_kses_post( '<span class="number" data-count="' . esc_html( $result[0]->total ) . '">' . esc_html( $result[0]->total ) . '</span>' );
	return ob_get_clean();
}
add_shortcode( 'total_votes', 'shortcode_count_total_votes' );

function shortcode_button_votes() {
	ob_start();
	echo wp_kses_post( '<p style="text-align: center;"><a class="btn-primary" style="font-size: 14px; padding: 11px 40px;" href="?vote=true">' . __( 'Bình chọn', '2vn' ) . '</a></p>' );
	return ob_get_clean();
}
add_shortcode( 'button_votes', 'shortcode_button_votes' );

function shortcode_button_dki() {
	ob_start();
	echo esc_html__( 'Đăng kí', '2vn' ) ;
	return ob_get_clean();
}
add_shortcode( 'button_dki', 'shortcode_button_dki' );

function shortcode_user_baiduthi() {
	ob_start();
	echo esc_html__( 'Tác giả', '2vn' ) ;
	return ob_get_clean();
}
add_shortcode( 'shortcode_user_baiduthi', 'shortcode_user_baiduthi' );

function shortcode_title_baiduthi() {
	ob_start();
	echo esc_html__( 'Bài dự thi', '2vn' ) ;
	return ob_get_clean();
}
add_shortcode( 'title_baiduthi', 'shortcode_title_baiduthi' );

function shortcode_filter_news_baiduthi(){
	ob_start();
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			?>
			<p class="filter-votes-link">
				<a href="?orderby=votes&order=DESC">Most voted</a>|
				<a href="?orderby=date">Latest vote</a>|
				<a href="?orderby=rand">Random</a>
			</p>
			<?php
		} else {
			?>
			<p class="filter-votes-link">
				<a href="?orderby=votes&order=DESC">Bình chọn nhiều nhất</a>|
				<a href="?orderby=date">Mới nhất</a>|
				<a href="?orderby=rand">Ngẫu nhiên</a>
			</p>
			<?php
		}
	}
	return ob_get_clean();
}
add_shortcode( 'filter_news_baiduthi', 'shortcode_filter_news_baiduthi' );

function shortcode_sidebar_2language(){
	ob_start();
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			?>
			<h2 class="widgettitle">2! Vietnam PHOTOGRAPHY & VIDEO Contest</h2>
			<div class="dky-content">
				<p><i class="far fa-user"></i> <a href="/en/entry-guidelines/">Entry Guidelines</a></p>
				<p><i class="far fa-user"></i> <?php echo do_shortcode( '[btn_tham_gia title="Sign Up"]' ); ?></p>
				<p><i class="far fa-heart"></i> How to vote</p>
				<p><i class="fas fa-award"></i> Prize</p>
				<?php echo do_shortcode( '[btn_tham_gia class="btn-primary"]' ); ?>
			</div>
			<?php
		} else {
			?>
			<h2 class="widgettitle">2! Vietnam PHOTOGRAPHY & VIDEO Contest</h2>
			<div class="dky-content">
				<p><i class="far fa-user"></i> <a href="/the-le-cuoc-thi/">Thể lệ cuộc thi</a></p>
				<p><i class="far fa-user"></i> <?php echo do_shortcode( '[btn_tham_gia title="Đăng ký tham gia"]' ); ?></p>
				<p><i class="far fa-heart"></i> Cách thức bình chọn</p>
				<p><i class="fas fa-award"></i> Giải thưởng</p>
				<?php echo do_shortcode( '[btn_tham_gia class="btn-primary"]' ); ?>
			</div>
			<?php
		}
	}
	return ob_get_clean();
}
add_shortcode( 'sidebar_2language', 'shortcode_sidebar_2language' );

function shortcode_title_widget_sidebar() {
	ob_start();
	echo esc_html__( 'BÀI VIẾT NỔI BẬT', '2vn' ) ;
	return ob_get_clean();
}
add_shortcode( 'title_widget_sidebar', 'shortcode_title_widget_sidebar' );

function shortcode_title_social_sidebar() {
	ob_start();
	echo esc_html__( 'KẾT NỐI', '2vn' ) ;
	return ob_get_clean();
}
add_shortcode( 'title_social_sidebar', 'shortcode_title_social_sidebar' );

function shortcode_title_related_post() {
	ob_start();
	echo esc_html__( 'BÀI VIẾT LIÊN QUAN', '2vn' ) ;
	return ob_get_clean();
}
add_shortcode( 'title_related_post', 'shortcode_title_related_post' );

function shortcode_footer_thongtin() {
	ob_start();
	echo esc_html__( '2! Vietnam là chương trình do Trung tâm Hỗ trợ Thanh niên Khởi nghiệp và Công ty cổ phần Truyền thông Sun Bright phối hợp tổ chức. Mọi thông tin liên hệ vui lòng gửi về:', '2vn' ) ;
	return ob_get_clean();
}
add_shortcode( 'footer_thongtin', 'shortcode_footer_thongtin' );

function shortcode_footer_address() {
	ob_start();
	echo esc_html__( 'Tầng 7, số 7 phố Xã Đàn, quận Đống Đa, Hà Nội', '2vn' ) ;
	return ob_get_clean();
}
add_shortcode( 'footer_address', 'shortcode_footer_address' );


function shortcode_footer_thele() {
	ob_start();
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			?>
			<p><a href="https://demo.2vietnam.vn/en/entry-guidelines/"><i class="fas fa-chevron-right"></i> Contest Rules</a></p>

			<p><a href="https://demo.2vietnam.vn/en/entry-enquiry/"><i class="fas fa-chevron-right"></i> How to enter</a></p>

			<p><a href="#"><i class="fas fa-chevron-right"></i> How to vote</a></p>

			<p><a href="#"><i class="fas fa-chevron-right"></i> Prizes</a></p>
			<?php
		}
		else {
			?>
			<p><a href="https://demo.2vietnam.vn/the-le-cuoc-thi/"><i class="fas fa-chevron-right"></i> Thể lệ tham dự</a></p>

			<p><a href="https://demo.2vietnam.vn/nop-bai-du-thi/"><i class="fas fa-chevron-right"></i> Hướng dẫn nộp bài</a></p>

			<p><a href="#"><i class="fas fa-chevron-right"></i> Hướng dẫn bình chọn</a></p>

			<p><a href="#"><i class="fas fa-chevron-right"></i> Giải thưởng</a></p>
			<?php
		}
	}
	return ob_get_clean();
}
add_shortcode( 'footer_thele', 'shortcode_footer_thele' );

function shortcode_footer_subcribe_title() {
	ob_start();
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			echo esc_html__( ' Subcribe for news', '2vn' ) ;
		}
		else {
			echo esc_html__( 'Đăng kí nhận tin', '2vn' ) ;
		}
	}
	return ob_get_clean();
}
add_shortcode( 'footer_subcribe_title', 'shortcode_footer_subcribe_title' );

function shortcode_title_form_nopbai() {
	ob_start();
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			echo esc_html__( 'Upload Images - Video', '2vn' ) ;
		}
		else {
			echo esc_html__( 'Tải hình ảnh - video', '2vn' ) ;
		}
	}
	return ob_get_clean();
}
add_shortcode( 'title_form_nopbai', 'shortcode_title_form_nopbai' );

function shortcode_theloai_form_nopbai() {
	ob_start();
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			echo esc_html__( 'Type', '2vn' ) ;
		}
		else {
			echo esc_html__( 'Thể loại nội dung', '2vn' ) ;
		}
	}
	return ob_get_clean();
}
add_shortcode( 'theloai_form_nopbai', 'shortcode_theloai_form_nopbai' );

function shortcode_theloaianh_form_nopbai() {
	ob_start();
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			echo esc_html__( 'Upload file (for image)', '2vn' ) ;
		}
		else {
			echo esc_html__( 'Tải lên tệp (đối với hình ảnh)', '2vn' ) ;
		}
	}
	return ob_get_clean();
}
add_shortcode( 'theloaianh_form_nopbai', 'shortcode_theloaianh_form_nopbai' );

function shortcode_theloaivideo_form_nopbai() {
	ob_start();
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			echo esc_html__( 'Paste your video link here', '2vn' ) ;
		}
		else {
			echo esc_html__( 'Dán link video của bạn vào đây nhé', '2vn' ) ;
		}
	}
	return ob_get_clean();
}
add_shortcode( 'theloaivideo_form_nopbai', 'shortcode_theloaivideo_form_nopbai' );

function shortcode_dieukhoan_form_nopbai() {
	ob_start();
	if ( function_exists( 'pll_current_language' ) ) {
		if ( 'English' === pll_current_language( 'name' ) ) {
			echo wp_kses_post( 'I agree with all <a class="open-popup" style="color: #f97300" href="#popup"> terms </a> of the organizer' );
		}
		else {
			echo wp_kses_post( 'Tôi đồng ý với mọi <a class="open-popup" style="color: #f97300" href="#popup"> điều khoản </a> của ban tổ chức' );
		}
	}
	return ob_get_clean();
}
add_shortcode( 'dieukhoan_form_nopbai', 'shortcode_dieukhoan_form_nopbai' );

/*
 * Test send email with mailtrap
 */
// function mailtrap( $phpmailer ) {
// 	$phpmailer->isSMTP();
// 	$phpmailer->Host = 'smtp.mailtrap.io';
// 	$phpmailer->SMTPAuth = true;
// 	$phpmailer->Port = 2525;
// 	$phpmailer->Username = '4cf0257bd977f2';
// 	$phpmailer->Password = '3d67e76e34af8e';
// }

// add_action( 'phpmailer_init', 'mailtrap' );


// Send email hợp lệ.
function enqueue() {
	$object = [
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'nonce'   => wp_create_nonce( 'send-email' ),
		'nonce_kohople' => wp_create_nonce( 'send-email-kohople' ),
		'nonce_binhchonthang' => wp_create_nonce( 'send-email-binhchonthang' ),
		'nonce_vongtrong' => wp_create_nonce( 'send-email-vongtrong' ),
		'nonce_workshop' => wp_create_nonce( 'send-email-workshop' ),
		'nonce_gala' => wp_create_nonce( 'send-email-gala' ),
	];
	$screen = get_current_screen();
	if ( is_admin() && ( $screen->id == 'bai-du-thi' ) ) {
		$object[ 'post_id' ] = get_the_ID();
	}
	wp_localize_script( 'hivn-ajax-script', 'ajax_object', $object );
}
add_action( 'admin_enqueue_scripts', 'enqueue', 9999 );


function send_email() {
	check_ajax_referer( 'send-email' );

	$post_id = $_POST['post_id'];
	$user_id = rwmb_meta( 'user', '', $post_id );
	$user    = get_userdata( $user_id );
	$name    = $user->first_name;
	$send_to = $user->user_email;

	ob_start();
	include get_stylesheet_directory() . '/template-email/email-hople.php';
	$body1 = ob_get_contents();
	ob_end_clean();

	add_filter( 'wp_mail_content_type', function() {
		return 'text/html';
	} );
	wp_mail( $send_to, __( '[2! Vietnam] Bài dự thi của bạn đã hợp lệ và được đăng tải/Congratulations on your entries has been successful and uploaded.', '2vn' ), $body1 );
	remove_filter( 'wp_mail_content_type', 'set_html_mail_content_type1' );
	wp_send_json_success( 'success' );
}
add_action( 'wp_ajax_send_email', 'send_email' );
add_action( 'wp_ajax_nopriv_send_email', 'send_email' );


// Send email không hợp lệ.
function send_email_kohople() {
	check_ajax_referer( 'send-email-kohople' );

	$post_id = $_POST['post_id'];
	$user_id = rwmb_meta( 'user', '', $post_id );
	$user    = get_userdata( $user_id );
	$name    = $user->first_name;
	$send_to = $user->user_email;
	$post_title = get_the_title( $post_id );

	ob_start();
	include get_stylesheet_directory() . '/template-email/email-khong-hople.php';
	$body1 = ob_get_contents();
	ob_end_clean();

	add_filter( 'wp_mail_content_type', function() {
		return 'text/html';
	} );
	wp_mail( $send_to, __( '[2! Vietnam] Bài dự thi của bạn chưa đủ thông tin hợp lệ/ 2! Vietnam announced: Your entries are missing information.', '2vn' ), $body1 );
	remove_filter( 'wp_mail_content_type', 'set_html_mail_content_type1' );
	wp_send_json_success( 'success' );
}
add_action( 'wp_ajax_send_email_kohople', 'send_email_kohople' );
add_action( 'wp_ajax_nopriv_send_email_kohople', 'send_email_kohople' );

// Send email giải bình chọn tháng.
function send_email_binhchonthang() {
	check_ajax_referer( 'send-email-binhchonthang' );

	$post_id = $_POST['post_id'];
	$user_id = rwmb_meta( 'user', '', $post_id );
	$votes   = (int) rwmb_meta( 'votes', '', $post_id );
	$user    = get_userdata( $user_id );
	$name    = $user->first_name;
	$send_to = $user->user_email;
	$post_title = get_the_title( $post_id );

	ob_start();
	include get_stylesheet_directory() . '/template-email/email-binhchonthang.php';
	$body1 = ob_get_contents();
	ob_end_clean();

	add_filter( 'wp_mail_content_type', function() {
		return 'text/html';
	} );
	wp_mail( $send_to, __( '[2! Vietnam] Chúc mừng bạn đã đạt giải thưởng bình chọn tháng/Congratulations your entries have won the voting award monthly.', '2vn' ), $body1 );
	remove_filter( 'wp_mail_content_type', 'set_html_mail_content_type1' );
	wp_send_json_success( 'success' );
}
add_action( 'wp_ajax_send_email_binhchonthang', 'send_email_binhchonthang' );
add_action( 'wp_ajax_nopriv_send_email_binhchonthang', 'send_email_binhchonthang' );

// Send email lọt vào vòng trong.
function send_email_vongtrong() {
	check_ajax_referer( 'send-email-vongtrong' );

	$post_id = $_POST['post_id'];
	$user_id = rwmb_meta( 'user', '', $post_id );
	$user    = get_userdata( $user_id );
	$name    = $user->first_name;
	$send_to = $user->user_email;
	$post_title = get_the_title( $post_id );

	ob_start();
	include get_stylesheet_directory() . '/template-email/email-vongtrong.php';
	$body1 = ob_get_contents();
	ob_end_clean();

	add_filter( 'wp_mail_content_type', function() {
		return 'text/html';
	} );
	wp_mail( $send_to, __( '[2! Vietnam] Chúc mừng tác phẩm của bạn đã lọt vào vòng tiếp theo/ Congratulations your entries have selected to reach the next round.', '2vn' ), $body1 );
	remove_filter( 'wp_mail_content_type', 'set_html_mail_content_type1' );
	wp_send_json_success( 'success' );
}
add_action( 'wp_ajax_send_email_vongtrong', 'send_email_vongtrong' );
add_action( 'wp_ajax_nopriv_send_email_vongtrong', 'send_email_vongtrong' );

// Send email mời tham gia Gala.
function send_email_gala() {
	check_ajax_referer( 'send-email-gala' );

	$post_id = $_POST['post_id'];
	$user_id = rwmb_meta( 'user', '', $post_id );
	$user    = get_userdata( $user_id );
	$name    = $user->first_name;
	$send_to = $user->user_email;
	$post_title = get_the_title( $post_id );

	ob_start();
	include get_stylesheet_directory() . '/template-email/email-gala.php';
	$body1 = ob_get_contents();
	ob_end_clean();

	add_filter( 'wp_mail_content_type', function() {
		return 'text/html';
	} );
	wp_mail( $send_to, __( '[2! Vietnam] Thư mời tham dự Gala Trao giải cuộc thi "2! Vietnam photography and video contest"/ The invitation to participate Gala award for "2! Vietnam photography and video contest".', '2vn' ), $body1 );
	remove_filter( 'wp_mail_content_type', 'set_html_mail_content_type1' );
	wp_send_json_success( 'success' );
}
add_action( 'wp_ajax_send_email_gala', 'send_email_gala' );
add_action( 'wp_ajax_nopriv_send_email_gala', 'send_email_gala' );

add_action( 'rwmb_enqueue_scripts', 'hivn_enqueue_custom_script' );
function hivn_enqueue_custom_script() {
	wp_enqueue_script( 'hivn-ajax-script', get_stylesheet_directory_uri() . '/js/admin.js', array( 'jquery' ), '', true );
}


// Custom search in bai-du-thi
function custom_search_query( $query ) {
	global $wpdb;
	$custom_fields = array(
		// put all the meta fields you want to search for here
		'user',
	);
	$searchterm = $query->query_vars['s'];
	// $user = $wpdb->get_results( "SELECT user_id from $wpdb->usermeta where meta_key = 'first_name' and meta_value ='" . $searchterm . "'" );
	$user = $wpdb->get_results( "SELECT user_id from $wpdb->usermeta inner join $wpdb->users on $wpdb->usermeta.user_id = $wpdb->users.ID where ( meta_key = 'first_name' and meta_value ='" . $searchterm . "') or ( meta_key = 'phone' and meta_value ='" . $searchterm . "') or user_email ='" . $searchterm . "'" );

	if ( empty( $user ) ) {
		return;
	}

	// we have to remove the "s" parameter from the query, because it will prevent the posts from being found.
	$query->query_vars['s'] = "";

	if ( ( '' !== $searchterm ) ) {
		$meta_query = array();
		foreach ( $custom_fields as $cf ) {
			array_push( $meta_query, array(
				'key'     => $cf,
				'value'   => $user[0]->user_id,
				'compare' => 'LIKE',
			) );
		}
		$query->set( 'post_type', 'bai-du-thi' );
		$query->set( 'meta_query', $meta_query );
	};
}
add_action( 'pre_get_posts', 'custom_search_query' );

// Form nộp bài
add_filter( 'rwmb_meta_boxes', 'hivn_register_form_nopbai_meta_boxes' );
function hivn_register_form_nopbai_meta_boxes( $meta_boxes ) {
	$meta_boxes[] = array (
		'title' => 'Upload hình ảnh - video',
		'id' => 'uploads',
		'post_types' => array(
			0 => 'bai-du-thi',
		),
		'context' => 'advanced',
		'priority' => 'high',
		'fields' => array(
			array (
				'id' => 'heading_3',
				'type' => 'heading',
				'name' => esc_html__( 'Tải hình ảnh - video', '2vn' ),
			),
			array (
				'id' => 'type',
				'type' => 'taxonomy',
				'name' => esc_html__( 'Thể loại nội dung', '2vn' ),
				'std' => 6,
				'taxonomy' => 'the-loai',
				'field_type' => 'radio_list',
				'inline' => true,
				'admin_columns' => array(
					'position' => 'after title',
				),
			),
			array (
				'id' => 'images',
				'type' => 'file',
				'name' => esc_html__( 'Tải lên tệp (đối với hình ảnh)', '2vn' ),
				'force_delete' => 1,
				'max_file_uploads' => 5,
				'columns' => 12,
				'upload_dir' => '/var/www/html/contest/',
				'visible' => array(
					'when' => array(
						array (
							0 => 'type',
							1 => '=',
							2 => 6,
						),
					),
					'relation' => 'and',
				),
			),
			array (
				'id' => 'video',
				'type' => 'oembed',
				'name' => esc_html__( 'Dán link video của bạn vào đây nhé', '2vn' ),
				'columns' => 12,
				'placeholder' => 'https://',
				'visible' => array(
					'when' => array(
						array (
							0 => 'type',
							1 => '=',
							2 => 7,
						),
					),
					'relation' => 'and',
				),
			),
			array (
				'id' => 'agree',
				'type' => 'checkbox',
				'desc' => __( 'Tôi đồng ý với mọi <a class="open-popup" style="color: #f97300" href="#popup"> điều khoản </a> của ban tổ chức', '2vn' ),
				'required' => 1,
			),
		),
		'style' => 'seamless',
		'validation' => array(
			'rules' => array(
				'type' => array(
					'required' => true,
				),
			),
		),
	);
	return $meta_boxes;
}

// Form thông tin User
add_filter( 'rwmb_meta_boxes', 'hivn_register_form_thongtinuser_meta_boxes' );
function hivn_register_form_thongtinuser_meta_boxes( $meta_boxes ) {
	$meta_boxes[] = array (
		'title' => 'Thông tin người dùng',
		'id' => 'user-info',
		'fields' => array(
			array (
				'id' => 'first_name',
				'type' => 'text',
				'name' => esc_html__( 'Họ và tên', '2vn' ),
			),
			array (
				'id' => 'user_email',
				'name' => 'Email',
				'type' => 'email',
			),
			array (
				'id' => 'phone',
				'type' => 'text',
				'name' => esc_html__( 'Số điện thoại', '2vn' ),
			),
			array (
				'id' => 'city',
				'name' => esc_html__( 'Tỉnh / Thành phố', '2vn' ),
				'type' => 'select_advanced',
				'placeholder' => '---',
				'options' => array(
					'Hà Nội' => 'Hà Nội',
					'Thành phố Hồ Chí Minh' => 'Thành phố Hồ Chí Minh',
					'An Giang' => 'An Giang',
					'Bà Rịa - Vũng Tàu' => 'Bà Rịa - Vũng Tàu',
					'Bình Dương' => 'Bình Dương',
					'Bình Phước' => 'Bình Phước',
					'Bình Thuận' => 'Bình Thuận',
					'Bình Định' => 'Bình Định',
					'Bạc Liêu' => 'Bạc Liêu',
					'Bắc Giang' => 'Bắc Giang',
					'Bắc Kạn' => 'Bắc Kạn',
					'Bắc Ninh' => 'Bắc Ninh',
					'Bến Tre' => 'Bến Tre',
					'Cao Bằng' => 'Cao Bằng',
					'Cà Mau' => 'Cà Mau',
					'Cần Thơ' => 'Cần Thơ',
					'Điện Biên' => 'Điện Biên',
					'Đà Nẵng' => 'Đà Nẵng',
					'Đắk Lắk' => 'Đắk Lắk',
					'Đắk Nông' => 'Đắk Nông',
					'Đồng Nai' => 'Đồng Nai',
					'Đồng Tháp' => 'Đồng Tháp',
					'Gia Lai' => 'Gia Lai',
					'Hoà Bình' => 'Hoà Bình',
					'Hà Giang' => 'Hà Giang',
					'Hà Nam' => 'Hà Nam',
					'Hà Tĩnh' => 'Hà Tĩnh',
					'Hưng Yên' => 'Hưng Yên',
					'Hải Dương' => 'Hải Dương',
					'Hải phòng' => 'Hải phòng',
					'Hậu Giang' => 'Hậu Giang',
					'Khánh Hòa' => 'Khánh Hòa',
					'Kiên Giang' => 'Kiên Giang',
					'Kon Tum' => 'Kon Tum',
					'Lai Châu' => 'Lai Châu',
					'Long An' => 'Long An',
					'Lào Cai' => 'Lào Cai',
					'Lâm Đồng' => 'Lâm Đồng',
					'Lạng Sơn' => 'Lạng Sơn',
					'Nam Định' => 'Nam Định',
					'Nghệ An' => 'Nghệ An',
					'Ninh Bình' => 'Ninh Bình',
					'Ninh Thuận' => 'Ninh Thuận',
					'Phú Thọ' => 'Phú Thọ',
					'Phú Yên' => 'Phú Yên',
					'Quảng Bình' => 'Quảng Bình',
					'Quảng Nam' => 'Quảng Nam',
					'Quảng Ngãi' => 'Quảng Ngãi',
					'Quảng Ninh' => 'Quảng Ninh',
					'Quảng Trị' => 'Quảng Trị',
					'Sóc Trăng' => 'Sóc Trăng',
					'Sơn La' => 'Sơn La',
					'Thanh Hóa' => 'Thanh Hóa',
					'Thái Bình' => 'Thái Bình',
					'Thái Nguyên' => 'Thái Nguyên',
					'Thừa Thiên Huế' => 'Thừa Thiên Huế',
					'Tiền Giang' => 'Tiền Giang',
					'Trà Vinh' => 'Trà Vinh',
					'Tuyên Quang' => 'Tuyên Quang',
					'Tây Ninh' => 'Tây Ninh',
					'Vĩnh Long' => 'Vĩnh Long',
					'Vĩnh Phúc' => 'Vĩnh Phúc',
					'Yên Bái' => 'Yên Bái',
				),
			),
			array (
				'id' => 'address',
				'type' => 'text',
				'name' => esc_html__( 'Địa chỉ liên hệ', '2vn' ),
			),
		),
		'type' => 'user',
		'validation' => array(
			'rules' => array(
				'first_name' => array(
					'required' => true,
				),
				'user_email' => array(
					'required' => true,
				),
				'phone' => array(
					'required' => true,
				),
				'city' => array(
					'required' => true,
				),
			),
		),
	);
	return $meta_boxes;
}