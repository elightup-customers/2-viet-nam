<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php do_action( 'fl_head_open' ); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php echo apply_filters( 'fl_theme_viewport', "<meta name='viewport' content='width=device-width, initial-scale=1.0' />\n" ); ?>
<?php echo apply_filters( 'fl_theme_xua_compatible', "<meta http-equiv='X-UA-Compatible' content='IE=edge' />\n" ); ?>
<link rel="profile" href="https://gmpg.org/xfn/11" />
<?php

wp_head();
?>

<body <?php body_class(); ?> itemscope="itemscope" itemtype="https://schema.org/WebPage">
	<div class="fl-page">
		<header class="longform-header">
			<a class="back-to-site" href="<?php echo esc_url( home_url() ); ?>">Back</a>
			<a class="fl-logo-img" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php FLTheme::fixed_header_logo(); ?></a>
			<div class="fb-like" style="text-align: center; padding-right: 20px; padding-bottom: 10px;" data-href="" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
		</header>
		<div class="fl-page-content" itemprop="mainContentOfPage">
		<?php do_action( 'fl_content_open' ); ?>
