<?php
class Form {
	private $auth;

	public function __construct( $auth ) {
		$this->auth = $auth;

		add_action( 'init', [ $this, 'register_shortcodes' ] );

		if ( ! is_admin() ) {
			add_filter( 'rwmb_file_add_string', [ $this, 'change_upload_text' ] );
			add_filter( 'rwmb_frontend_post_content', [ $this, 'change_post_content' ] );
			add_filter( 'rwmb_frontend_post_title', [ $this, 'change_post_title' ] );
			add_filter( 'rwmb_frontend_redirect', [ $this, 'redirect' ], 10, 2 );
		}

		add_action( 'rwmb_frontend_after_process', [ $this, 'save_data' ], 9, 2 );
		add_action( 'rwmb_frontend_before_form', [ $this, 'catch_form' ] );
		add_action( 'rwmb_frontend_after_form', [ $this, 'flush_form_cache' ] );
		add_action( 'rwmb_frontend_after_process', [ $this, 'send_email' ], 10, 2 );
	}

	public function register_shortcodes() {
		add_shortcode( 'submit_form', [ $this, 'submit_form_shortcode' ] );
	}

	public function submit_form_shortcode() {
		// For webview in the chat bot, `_id` must set.
		$id = filter_input( INPUT_GET, '_id', FILTER_SANITIZE_STRING );
		if ( $id && 1 === $this->auth->authenticate( $id ) ) {
			return '<p>' . __( 'Lỗi không xác thực được người dùng.', '2vn' ) . '</p>';
		}

		// After authentication in webview, or in normal browser, user must log in.
		if ( ! $this->auth->is_logged_in() ) {
			return '<p>' . __( 'Bạn phải đăng nhập để nộp bài dự thi.', '2vn' ) . '</p>';
		}

		$content = '';
		if ( ! filter_input( INPUT_GET, 'rwmb-form-submitted' ) ) {
			$content = '<p>' . __( 'Vui lòng điền đầy đủ các thông tin sau để nộp bài dự thi. Mọi thắc mắc vui lòng liên hệ hỗ trợ tại hotline của chúng tôi. Lưu ý, các mục đánh dấu sao (*) là bắt buộc phải điền.', '2vn' ) . '</p>';
		}
		$button        = __( 'Gửi bài', '2vn' );
		$confirmation  = __( 'Bài dự thi của bạn đã được gửi thành công!', '2vn' );
		$content       .= do_shortcode( '[mb_frontend_form id="uploads" post_fields="title,content" post_status="pending" submit_button="' . $button . '" confirmation="' . $confirmation . '"]' );
		return $content;
	}

	public function change_upload_text() {
		return __( '+ Thêm file', '2vn' );
	}

	public function change_post_content( $field ) {
		$field['name']    = __( 'Chia sẻ trải nghiệm của bạn (không quá 500 từ)', '2vn' );
		$field['options'] = [
			'media_buttons' => false,
			'tinymce'       => false,
			'quicktags'     => false,
			'textarea_rows' => 8,
		];
		$field['after']   = '</div>'; // .box
		return $field;
	}

	public function change_post_title( $field ) {
		$field['name']     = __( 'Tên tác phẩm dự thi', '2vn' ) . ' <span class="rwmb-required">*</span>';
		$field['required'] = true;
		$field['before'] = '<div class="box">';
		return $field;
	}

	public function redirect( $redirect, $config ) {
		if ( 'uploads' !== $config['id'] ) {
			return $redirect;
		}

		$id = filter_input( INPUT_GET, '_id', FILTER_SANITIZE_STRING );
		if ( $id && 1 === $this->auth->authenticate( $id ) ) {
			$message = '<p>' . esc_html__( 'Lỗi không xác thực được người dùng. Vui lòng liên hệ với quản trị viên.', '2vn' ) . '</p>';
			error( $message );
		}

		$user = $this->auth->get_user();
		if ( null === $user ) {
			$message = '<p>' . esc_html__( 'Lỗi không xác thực được người dùng. Vui lòng liên hệ với quản trị viên.', '2vn' ) . '</p>';
			error( $message );
		}

		$this->notify_chat_bot( $this->auth->get_user_id() );

		$query = new WP_Query( [
			'post_type'              => 'bai-du-thi',
			'post_status'            => 'any',
			'meta_key'               => 'user',
			'meta_value'             => $user->ID,
			'posts_per_page'         => 2,
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'fields'                 => 'ids',
			'lang'                   => 'vi',
		] );
		if ( 2 === count( $query->posts ) ) {
			return $redirect;
		}

		if ( function_exists( 'pll_current_language' ) ) {
			if ( 'English' === pll_current_language( 'name' ) ) {
				$url = 'https://demo.2vietnam.vn/en/dang-ky-thong-tin-2/';
			} else {
				$url = 'https://demo.2vietnam.vn/dang-ky-thong-tin/';
			}
		}

		if ( $id ) {
			$url = add_query_arg( '_id', $id, 'https://demo.2vietnam.vn/dang-ky-thong-tin-webview/' );
		}
		return $url;
	}

	private function notify_chat_bot( $id ) {
		$data = [
			'_id' => $id,
		];

		post( 'sendBlockSuccessfulSubmission', $data, false );
	}

	public function save_data( $config, $post_id ) {
		if ( 'uploads' !== $config['id'] ) {
			return;
		}

		$user = $this->auth->get_user();
		if ( ! $user ) {
			return;
		}

		// Set user.
		update_post_meta( $post_id, 'user', $user->ID );
		update_post_meta( $post_id, 'votes', 0 );

		// Set month.
		$month = date( 'n' );
		$terms = [ 'vong-so-loai', "thang-$month" ] ;
		wp_set_object_terms( $post_id, $terms, 'vong-thi' );

		$this->generate_thumbs( $post_id );

		// Set language = vi
		if ( function_exists( 'pll_set_post_language' ) ) {
			pll_set_post_language( $post_id, 'vi' );
		}
	}

	private function generate_thumbs( $post_id ) {
		$images = get_post_meta( $post_id, 'images', false );
		if ( empty( $images ) ) {
			return;
		}

		array_walk( $images, [ $this, 'generate_thumbs_for_image' ] );
	}

	private function generate_thumbs_for_image( $image ) {
		$dst = ABSPATH . '/images/';

		$src   = $this->url_to_path( $image );
		$info  = pathinfo( $src );
		$large = $info['filename'] . '-lg.' . $info['extension'];
		$small = $info['filename'] . '-sm.' . $info['extension'];

		hivn_image_resize( $src, $dst . $large, 770, 490, false );
		hivn_image_resize( $src, $dst . $small, 237, 140 );
	}

	private function url_to_path( $url ) {
		return str_replace( home_url( '/' ), trailingslashit( ABSPATH ), $url );
	}

	public function catch_form() {
		if ( filter_input( INPUT_GET, 'rwmb-form-submitted' ) ) {
			ob_start();
		}
	}

	public function flush_form_cache() {
		if ( filter_input( INPUT_GET, 'rwmb-form-submitted' ) ) {
			ob_end_clean();
		}
	}

	public function send_email( $config, $post_id ) {

		if ( 'uploads' !== $config['id'] ) {
			return;
		}

		$user = $this->auth->get_user();
		if ( ! $user ) {
			return;
		}

		$name = $user->first_name;
		$send_to = $user->user_email;
		$phone = $user->phone;
		$city = $user->city;

		ob_start();
		if ( function_exists( 'pll_current_language' ) ) {
			if ( 'English' === pll_current_language( 'name' ) ) {
				$language = 'english';
			} else {
				$language = 'tiengviet';
			}
		}
		if ( $language === 'english' ) {
			include get_stylesheet_directory() . '/template-email/email-success.php';
		} else {
			include get_stylesheet_directory() . '/template-email/email-success-tiengviet.php';
		}

		$body = ob_get_contents();
		// $body = 'Xin chào ' . $name . '<br> CẢM ƠN BẠN ĐÃ THAM GIA CUỘC THI 2!Vietnam Photo and Video Contest';

		ob_end_clean();

		add_filter( 'wp_mail_content_type', function() {
			return 'text/html';
		} );

		if ( $language === 'english' ) {
			$title = '[2! Vietnam] Congratulations on your successful submission';
		} else {
			$title = '[2! Vietnam] Chúc mừng bạn đã nộp bài thành công.';
		}

		wp_mail( $send_to, $title, $body );

		remove_filter( 'wp_mail_content_type', 'set_html_mail_content_type' );

	}

}
