<?php
/**
 * Debug & helper functions. DO NOT REMOVE.
 */

define( 'API_URL', 'https://api2vn.herokuapp.com/' );
define( 'API_KEY', 'kyc2vietnam' );

function d( $data ) {
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}
	echo '<pre>';
	print_r( $data );
	echo '</pre>';
	error_log( print_r( $data, true ) );
}
function v( $data ) {
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}
	echo '<pre>';
	var_dump( $data );
	echo '</pre>';
	error_log( print_r( $data, true ) );
}
function dd( $data ) {
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}
	echo '<pre>';
	print_r( $data );
	echo '</pre>';
	error_log( print_r( $data, true ) );
	die;
}
function vd( $data ) {
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}
	echo '<pre>';
	var_dump( $data );
	echo '</pre>';
	error_log( print_r( $data, true ) );
	die;
}

function post( $endpoint, $data, $die = true ) {
	return request( 'POST', $endpoint, $data, $die );
}

function get( $endpoint, $data, $die = true ) {
	return request( 'GET', $endpoint, $data, $die );
}

function request( $method, $endpoint, $data, $die = true ) {
	$data['Key'] = API_KEY;

	$func = 'POST' === $method ? 'wp_remote_post' : 'wp_remote_get';

	$params = [
		'headers' => [ 'Content-Type' => 'application/json; charset=utf-8' ],
		'timeout' => 30,
	];
	if ( 'POST' === $method ) {
		$params['body']        = json_encode( $data );
		$params['data_format'] = 'body';
	} else {
		$params['body'] = $data;
	}

	$response = $func( API_URL . $endpoint, $params );

	$body = wp_remote_retrieve_body( $response );

	$data = json_decode( $body );

	$is_error = false;
	if ( null === $data ) {
		$is_error = true;
	}
	if ( isset( $data->Code ) && 'ERROR' === $data->Code ) {
		$is_error = true;
	}

	if ( ! $is_error ) {
		return $data;
	}

	if ( ! $die ) {
		return;
	}

	if ( null === $data ) {
		d( $endpoint );
		d( $params );
		d( $response );
	} else {
		d( $endpoint );
		d( $params );
		d( $data );
	}

	$message = '<p>' . esc_html__( 'Có lỗi xảy ra. Hãy thông báo cho quản trị viên biết để khắc phục.', '2vn' ) . '</p>';
	if ( isset( $data->Mess ) ) {
		$message = '<p>' . esc_html__( 'Có lỗi xảy ra: ', '2vn' ) . $data->Mess . '.</p>';
	}
	error( $message );
}

function error( $message ) {
	if ( empty( $_SERVER['HTTP_REFERER'] ) ) {
		$url = home_url( '/' );
		$text = __( 'Trở về trang chủ', '2vn' );
	} else {
		$url = $_SERVER['HTTP_REFERER'];
		$text = __( 'Trở về trang trước', '2vn' );
	}

	$message .= '<p><a href="' . esc_url( $url ) . '">' . esc_html( $text ) . ' &rarr;</a></p>';
	wp_die( $message, esc_html__( 'Có lỗi xảy ra' ) );
}

function hivn_image_resize( $src, $dst, $width, $height, $crop = true ) {

	if ( ! list($w, $h) = getimagesize( $src ) ) {
		return 'Unsupported picture type!';
	}

	$type = strtolower( substr( strrchr( $src, '.' ), 1 ) );
	if ( $type == 'jpeg' ) {
		$type = 'jpg';
	}
	switch ( $type ) {
		case 'bmp':
			$img = imagecreatefromwbmp( $src );
			break;
		case 'gif':
			$img = imagecreatefromgif( $src );
			break;
		case 'jpg':
			$img = imagecreatefromjpeg( $src );
			break;
		case 'png':
			$img = imagecreatefrompng( $src );
			break;
		default:
			return 'Unsupported picture type!';
	}

	// resize
	if ( $crop ) {
		if ( $w < $width or $h < $height ) {
			$width  = $w;
			$height = $h;
			$x      = 0;
		} else {
			$ratio = max( $width / $w, $height / $h );
			$h     = $height / $ratio;
			$x     = ( $w - $width / $ratio ) / 2;
			$w     = $width / $ratio;
		}
	} else {
		if ( $w < $width and $h < $height ) {
			$width  = $w;
			$height = $h;
			$x      = 0;
		} else {
			$ratio  = min( $width / $w, $height / $h );
			$width  = $w * $ratio;
			$height = $h * $ratio;
			$x      = 0;
		}
	}

	$new = imagecreatetruecolor( $width, $height );

	// preserve transparency
	if ( $type == 'gif' or $type == 'png' ) {
		imagecolortransparent( $new, imagecolorallocatealpha( $new, 0, 0, 0, 127 ) );
		imagealphablending( $new, false );
		imagesavealpha( $new, true );
	}

	imagecopyresampled( $new, $img, 0, 0, $x, 0, $width, $height, $w, $h );

	$stamp = imagecreatefrompng( 'https://demo.2vietnam.vn/wp-content/themes/bb-theme-child/images/LOGO_WATERMARK.png' );
	// Set the margins for the stamp and get the height/width of the stamp image
	// $marge_right = 10;
	// $marge_bottom = 10;
	$sx = imagesx( $stamp );
	$sy = imagesy( $stamp );

	// Copy the stamp image onto our photo using the margin offsets and the photo
	// width to calculate positioning of the stamp.
	if ( $crop == false ) {
		imagecopy( $new, $stamp, ( imagesx( $new ) - $sx )/2, ( imagesy( $new ) - $sy )/2, 0, 0, imagesx( $stamp ), imagesy( $stamp ) );
	}

	switch ( $type ) {
		case 'bmp':
			imagewbmp( $new, $dst );
			break;
		case 'gif':
			imagegif( $new, $dst );
			break;
		case 'jpg':
			imagejpeg( $new, $dst );
			break;
		case 'png':
			imagepng( $new, $dst );
			break;
	}

	return true;
}

function change_username( $old_username, $new_username ) {
	global $wpdb;

	// do nothing if old username does not exist.
	$user_id = username_exists( $old_username );
	if ( ! $user_id ) {
		return false;
	}

	// change username
	$q  = $wpdb->prepare( "UPDATE $wpdb->users SET user_login = %s WHERE user_login = %s", $new_username, $old_username );
	$wpdb->query($q);

	// change nicename if needed
	$q = $wpdb->prepare( "UPDATE $wpdb->users SET user_nicename = %s WHERE user_login = %s AND user_nicename = %s", $new_username, $new_username, $old_username );
	$wpdb->query($q);

	// change display name if needed
	$q  = $wpdb->prepare( "UPDATE $wpdb->users SET display_name = %s WHERE user_login = %s AND display_name = %s", $new_username, $new_username, $old_username );
	$wpdb->query($q);

	// when on multisite, check if old username is in the `site_admins` options array. if so, replace with new username to retain superadmin rights.
	if ( is_multisite() ) {
		$super_admins = (array) get_site_option( 'site_admins', array( 'admin' ) );
		$array_key = array_search( $old_username, $super_admins );
		if( $array_key ) {
			$super_admins[ $array_key ] = $new_username;
		}

		update_site_option( 'site_admins' , $super_admins );
	}

	return true;
}

function send_email_confirm( $send_to, $name, $language, $link_confirm = '' ) {
	// $name = $user->first_name;
	// $send_to = $user->user_email;
	// $name = '';
	ob_start();

	include get_stylesheet_directory() . '/template-email/email-xacthuc-' . $language . '.php';
	$body = ob_get_contents();

	ob_end_clean();

	add_filter( 'wp_mail_content_type', function() {
		return 'text/html';
	} );

	if ( function_exists( 'pll_current_language' ) ) {
		if ( $language === 'english' ) {
			$title = '[2! Vietnam] Welcome to 2! Vietnam. Please access your account verification information.';
		} else {
			$title = '[2! Vietnam] Chào mừng bạn đến với 2! Vietnam. Mời bạn truy cập thông tin xác thực tài khoản.';
		}
	}

	wp_mail( $send_to, $title, $body );

	remove_filter( 'wp_mail_content_type', 'set_html_mail_content_type' );
}

// Test API.
add_action( 'template_redirect', function () {
	$test     = filter_input( INPUT_GET, 'test', FILTER_SANITIZE_STRING );
	$method   = filter_input( INPUT_GET, 'method', FILTER_SANITIZE_STRING );
	$endpoint = filter_input( INPUT_GET, 'endpoint', FILTER_SANITIZE_STRING );

	if ( 'true' !== $test || ! $method || ! $endpoint ) {
		return;
	}

	$data = $_GET;
	unset( $data['test'], $data['method'], $data['endpoint'] );

	$result = request( $method, $endpoint, $data );
	dd( $result );
} );


// Change username at admin column.
add_filter( 'rwmb_user_choice_label', function ( $label, $field, $user ) {
	$first_name = get_user_meta( $user->ID, 'first_name', true );
    return $first_name ?: $label;
}, 10, 3 );
add_filter( 'rwmb_the_value', function ( $output, $field, $args, $post_id ) {
	if ( ! is_admin() ) {
		return $output;
	}
	$screen = get_current_screen();
	if ( 'edit-bai-du-thi' !== $screen->id ) {
		return $output;
	}
	if ( 'user' !== $field['id'] ) {
		return $output;
	}


	$user_id = get_post_meta( $post_id, 'user', true );
	$user = get_userdata( $user_id );
	$first_name = $user->first_name;

	return $first_name ?: strip_tags( $output );
}, 10, 4 );
