<?php
class Image {
	public function get_images( $post_id ) {
		$images = get_post_meta( $post_id, 'images', false );
		if ( empty( $images ) ) {
			return [];
		}

		$images = array_map( [ $this, 'get_public_urls' ], $images );
		return $images;
	}

	private function get_public_urls( $image ) {
		$dst = ABSPATH . '/images/';

		$src   = $this->url_to_path( $image );
		$info  = pathinfo( $src );
		$large = $info['filename'] . '-lg.' . $info['extension'];
		$small = $info['filename'] . '-sm.' . $info['extension'];

		$large = $this->path_to_url( $dst . $large );
		$small = $this->path_to_url( $dst . $small );

		return [
			'large' => $large,
			'small' => $small,
		];
	}

	private function url_to_path( $url ) {
		return str_replace( home_url( '/' ), trailingslashit( ABSPATH ), $url );
	}

	private function path_to_url( $path ) {
		$path          = wp_normalize_path( untrailingslashit( $path ) );
		$root          = wp_normalize_path( untrailingslashit( ABSPATH ) );
		$relative_path = str_replace( $root, '', $path );

		return home_url( $relative_path );
	}
}
