<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package FLChildTheme
 */

/**
 * FLChildTheme Recent Posts
 */
class FLChildTheme_Recent_Posts_Widget extends WP_Widget {
	/**
	 * Default widget options.
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Widget setup.
	 */
	public function __construct() {
		$this->defaults = array(
			'title'        => esc_html__( 'Bài mới nhất', 'fl-child-theme' ),
			'title_length' => 0,
			'number'       => 4,
			'type'         => 'latest',
			'category'     => -1,
		);

		parent::__construct(
			'fl-recent-posts',
			esc_html__( 'FLChildTheme: Bài viết mới', 'fl-child-theme' ),
			array(
				'classname'   => 'fl_recent_posts',
				'description' => 'A widget that displays your recent posts from all categories or a category',
			)
		);
	}

	/**
	 * Widget form.
	 *
	 * @param array $instance Widget instance.
	 *
	 * @return void
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		$title        = $instance['title'];
		$title_length = $instance['title_length'];
		$number       = $instance['number'];
		$type         = $instance['type'];
		$category     = $instance['category'];
		?>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title: ', 'fl-child-theme' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo $title; // WPCS XSS: OK. ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title_length' ) ); ?>"><?php esc_html_e( 'Độ dài tiêu đề bài viết: ', 'fl-child-theme' ); ?></label>
			<input type="number" id="<?php echo esc_attr( $this->get_field_id( 'title_length' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title_length' ) ); ?>" type="text" value="<?php echo $title_length; // WPCS XSS: OK. ?>" />
			<br>
			<i class="tiny-text"><?php esc_html_e( '(Đặt 0 để lấy hết tiêu đề)', 'fl-child-theme' ); ?></i>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Số bài viết hiển thị: ', 'fl-child-theme' ); ?></label>
			<input type="number" min="1" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo $number; // WPCS XSS: OK. ?>" size="3" />
		</p>
		<p>
			<input type="radio" <?php checked( $type, 'latest' ); ?> id="<?php echo esc_attr( $this->get_field_id( 'type' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'type' ) ); ?>" value="latest"/><?php esc_html_e( 'Hiển thị bài viết mới nhất', 'fl-child-theme' ); ?><br />
			<input type="radio" <?php checked( $type, 'category' ); ?> id="<?php echo esc_attr( $this->get_field_id( 'type' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'type' ) ); ?>" value="category"/><?php esc_html_e( 'Hiển thị từ chuyên mục', 'fl-child-theme' ); ?><br />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"><?php esc_html_e( 'Chọn chuyên mục: ', 'fl-child-theme' ); ?></label>
			<?php
			wp_dropdown_categories(
				array(
					'show_option_none' => ' ',
					'name'             => $this->get_field_name( 'category' ),
					'selected'         => $category,
				)
			);
			?>
		</p>
		<?php
	}

	/**
	 * Update the widget settings.
	 *
	 * @param array $new_instance New widget instance.
	 * @param array $old_instance Old widget instance.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title']        = sanitize_text_field( $new_instance['title'] );
		$instance['title_length'] = absint( $new_instance['title_length'] );
		$instance['number']       = absint( $new_instance['number'] );
		$instance['type']         = strip_tags( $new_instance['type'] );
		$instance['category']     = intval( $new_instance['category'] );
		return $instance;
	}

	/**
	 * How to display the widget on the screen.
	 *
	 * @param array $args     Widget parameters.
	 * @param array $instance Widget instance.
	 */
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		$title        = $instance['title'];
		$title_length = $instance['title_length'];
		$number       = $instance['number'];
		$type         = $instance['type'];
		$category     = $instance['category'];

		$query_args = array(
			'posts_per_page'      => $number,
			'post_type'           => 'post',
			'ignore_sticky_posts' => true,
		);
		if ( 'category' === $type ) {
			$query_args['category__in'] = $category;
		}
		$query = new WP_Query( $query_args );

		echo $args['before_widget']; // WPCS XSS: OK.
		if ( $title ) {
			echo $args['before_title'], $title , $args['after_title']; // WPCS: XSS OK.
		}

		while ( $query->have_posts() ) :
			$query->the_post();
			?>
			<?php
				$cat_ids         = get_the_ID();
				$cat_names_array = get_the_category( $cat_ids );
			?>
			<article <?php post_class( 'recent-post' ); ?>>
				<?php if ( has_post_thumbnail() ) : ?>
					<div class="recent-post__image">
						<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
							<?php the_post_thumbnail( 'related-image' ); ?>
						</a>
					</div>
				<?php endif; ?>
				<div class="recent-post__text">
					<h3 class="entry-title">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<?php
							if ( 0 === $title_length ) {
								$title = get_the_title();
							} else {
								$title = wp_trim_words( get_the_title(), $title_length, esc_html__( '...', 'fl-child-theme' ) );
							}
							echo esc_html( $title );
							?>
						</a>
					</h3>
					<div class="entry-meta">
						<span class="entry-category">
							<?php
							foreach ( $cat_names_array as $key => $value ) {
								echo esc_html( $value->name ) . ' -';
							}
							?>
						</span>
						<?php
						flchild_posted_on();
						?>
					</div>
				</div>
			</article>
			<!-- <hr width="150px;" style="margin: 15px 0; background-color: #e2e2e2;"> -->
			<?php
		endwhile;

		wp_reset_postdata();

		echo $args['after_widget']; // WPCS XSS: OK.
	}
}
