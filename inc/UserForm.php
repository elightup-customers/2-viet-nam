<?php
class UserForm {
	private $auth;

	public function __construct( $auth ) {
		$this->auth = $auth;

		add_action( 'init', [ $this, 'register_shortcodes' ] );

		add_action( 'rwmb_profile_user', [ $this, 'set_user' ], 10, 3 );
		add_action( 'rwmb_profile_after_process', [ $this, 'notify_chat_bot' ], 10, 2 );
		add_action( 'rwmb_profile_before_form', [ $this, 'catch_form' ] );
		add_action( 'rwmb_profile_after_form', [ $this, 'flush_form_cache' ] );
	}

	public function register_shortcodes() {
		add_shortcode( 'user_form', [ $this, 'user_form' ] );
		add_shortcode( 'user_baiduthi', [ $this, 'user_baiduthi' ] );
	}

	public function user_form() {
		// For webview in the chat bot, `_id` must set.
		$id = filter_input( INPUT_GET, '_id', FILTER_SANITIZE_STRING );
		if ( $id && 1 === $this->auth->authenticate( $id ) ) {
			return '<p>' . __( 'Lỗi không xác thực được người dùng.', '2vn' ) . '</p>';
		}

		// After authentication in webview, or in normal browser, user must log in.
		if ( ! $this->auth->is_logged_in() ) {
			return '<p>' . __( 'Bạn phải đăng nhập để cập nhật thông tin.', '2vn' ) . '</p>';
		}

		$content = '';
		if ( ! filter_input( INPUT_GET, 'rwmb-form-submitted' ) ) {
			$content = '<p>' . __( 'Vui lòng điền đầy đủ các thông tin dưới đây. Các thông tin bạn cung cấp sẽ rất hữu ích để ban tổ chức liên hệ với bạn trong các vòng thi tiếp theo. Lưu ý: các mục đánh dấu sao (*) là bắt buộc phải điền', '2vn' ) . '</p>';
		}
		$button        = __( 'Cập nhật', '2vn' );
		$confirmation  = __( 'Thông tin của bạn đã được cập nhật thành công!', '2vn' );
		$content       .= do_shortcode( '[mb_user_profile_info id="user-info" label_submit="' . $button . '" confirmation="' . $confirmation . '"]' );
		return $content;
	}

	public function user_baiduthi(){
		// For webview in the chat bot, `_id` must set.
		$id = filter_input( INPUT_GET, '_id', FILTER_SANITIZE_STRING );
		if ( $id && 1 === $this->auth->authenticate( $id ) ) {
			return '<p>' . __( 'Lỗi không xác thực được người dùng.', '2vn' ) . '</p>';
		}

		// After authentication in webview, or in normal browser, user must log in.
		if ( ! $this->auth->is_logged_in() ) {
			return;
		}
		$user_login = $this->auth->get_user_id();
		$wp_user = get_user_by( 'login', $user_login );
		$user_login_id = $wp_user->ID;
		$arg = array(
			'post_type'   => 'bai-du-thi',
			'lang'        => 'vi',
			'post_status' => 'any',
			'meta_query'  => array(
				array(
					'key' => 'user',
					'value' => $user_login_id,
				),
			),
		);
		$content = '<h3 style="text-align: center; margin-top: 70px;"><strong>' . __( 'Danh sách bài dự thi đã nộp', '2vn' ) . '</strong></h3>';
		$content .= '<div class="list-baiduthi">';
		$query = new WP_Query( $arg );

		if ( $query->have_posts() ) :
			while ( $query->have_posts() ) :
				$query->the_post();
				$content .= '<div class="baiduthi"><div class="wv-thumbnail">' . do_shortcode( '[vote_thumb]' ) . '</div>' . do_shortcode( '[vote_thumb_gallery]' ) . '<div class="wv-tex"><h2><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2><div class="wv-status">' . __( 'Tình trạng: ', '2vn' ) . '<span class="wv-vote-number">' . get_post_status() . '</span></div>' . do_shortcode( '[vote_list]' ) . '</div></div>';
			endwhile;
		else:
		$content .= '<span>Chưa có bài dự thi nào</span>';
		endif;
		$content .= '</div>';
		return $content;
	}

	public function set_user( $user, $meta_boxes, $args ) {
		if ( 'user-info' !== $args['id'] ) {
			return;
		}
		$user_login = $this->auth->get_user_id();
		$wp_user = get_user_by( 'login', $user_login );
		$user->set_user_id( $wp_user->ID );

		foreach ( $meta_boxes as $meta_box ) {
			$meta_box->set_object_id( $wp_user->ID );
		}
	}

	public function notify_chat_bot( $config, $user_id ) {
		if ( 'user-info' !== $config['id'] ) {
			return;
		}
		$user = get_userdata( $user_id );
		$data = [
			'_id'   => $user->user_login,
			'Name'  => $user->first_name,
			'Email' => $user->user_email,
			'Phone' => $user->phone,
			'City'  => $user->city,
		];
		post( 'updateMembers', $data, false );
	}

	public function catch_form() {
		if ( filter_input( INPUT_GET, 'rwmb-form-submitted' ) ) {
			ob_start();
		}
	}

	public function flush_form_cache() {
		if ( filter_input( INPUT_GET, 'rwmb-form-submitted' ) ) {
			ob_end_clean();
		}
	}
}
