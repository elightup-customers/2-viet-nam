<?php
class GoogleRegister {
	const CLIENT_ID = '206826321640-0jidccv02tlmjrf59ed6fd0533a20bmn.apps.googleusercontent.com';
	const CLIENT_SECRET = 'e1riD56iYikCjSO98EEH89sX';

	private $auth;

	public function __construct( $auth ) {
		$this->auth = $auth;

		add_action( 'init', [ $this, 'register_shortcodes'] );
		add_action( 'template_redirect', [ $this, 'process' ] );
	}

	public function register_shortcodes() {
		add_shortcode( 'google_login', [ $this, 'google_login_shortcode' ] );
	}

	public function google_login_shortcode() {
		$client = $this->get_google_client();
		return $client->createAuthUrl();
	}

	public function process() {
		if ( 'true' === filter_input( INPUT_GET, 'google_login', FILTER_SANITIZE_STRING ) ) {
			$this->login();
		}
	}

	private function login() {
		$client = $this->get_google_client();

		// If $_GET['code'] is empty, redirect user to google authentication page for code.
		// Code is required to aquire Access Token from google
		// Once we have access token, assign token to session variable
		// and we can redirect user back to page and login.
		if ( empty( $_GET['code'] ) ) {
			return;
		}
		$client->authenticate( $_GET['code'] );
		$_SESSION['access_token'] = $client->getAccessToken();

		$service = new Google_Service_Oauth2( $client );
		$user = $service->userinfo->get();

		$status = $this->register( $user );
		if ( 1 !== $status ) {
			$user_data = get_user_by( 'email', $user->email );
			$this->auth->set_user_id( $user_data->user_login );
		}

		$home_url = home_url();
		if ( function_exists( 'pll_current_language' ) ) {
			if ( 'English' === pll_current_language( 'name' ) ) {
				$text = 'You have successfully authenticated via Google. Please click <a href="' . $home_url . '">here</a> to return to the home page';
			} else {
				$text = 'Bạn đã xác thực qua Google thành công. Vui lòng click vào <a href="' . $home_url . '">đây</a> để trở về trang chủ';
			}
		}
		wp_die( $text );

		// $url = add_query_arg( 'status', $status );
		// wp_safe_redirect( $url );
		// die;
	}

	/**
	 * Register an user
	 *
	 * @param object $user User object from Google.
	 * @return int 1 = cannot register user, 2 = register succesffully, 3 = user exists.
	 */
	private function register( $user ) {
		if ( username_exists( $user->id ) || email_exists( $user->email ) ) {
			return 3;
		}

		$user_data = [
			'user_login' => $user->id,
			'user_pass'  => wp_generate_password(),
			'user_email' => $user->email,
			'first_name' => $user->name,
		];

		$result = wp_insert_user( $user_data );
		if ( is_wp_error( $result ) ) {
			return 1;
		}

		if ( ! empty( $user->picture ) ) {
			update_user_meta( $result, 'google_profile_image', $user->picture );
		}

		$this->send_user_data( $user_data );

		// gui email
		if ( function_exists( 'pll_current_language' ) ) {
			if ( 'English' === pll_current_language( 'name' ) ) {
				$language = 'english';
			} else {
				$language = 'tiengviet';
			}
		}
		$send_to = $user_data->user_email;
		$name    = $user_data->user_email;
		send_email_confirm( $send_to, $name, $language );

		return 2;
	}

	private function send_user_data( $user_data ) {
		$data = [
			'Name'       => $user_data['first_name'],
			'Type'       => 'Member',
			'OriginId'   => $user_data['user_login'],
			'Origin'     => 'Google',
			'Language'   => 'VN',
			'Email'      => $user_data['user_email'],
		];
		$result = post( 'saveMembers', $data );

		// Update username to `_id` from chatbot.
		change_username( $user_data['user_login'], $result->Data->_id );
	}

	private function get_google_client() {
		$client = new Google_Client();
		$client->setClientId( self::CLIENT_ID );
		$client->setClientSecret( self::CLIENT_SECRET);

		$redirect_uri = add_query_arg( 'google_login', 'true', home_url( '/' ) );
		$client->setRedirectUri( $redirect_uri );
		$client->addScope( 'email' );
		$client->addScope( 'profile' );

		return $client;
	}
}