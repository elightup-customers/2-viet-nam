<?php
class Vote {
	private $auth;
	private $user;
	private $user_id;

	public function __construct( $auth ) {
		$this->auth = $auth;

		add_action( 'init', [ $this, 'init' ] );
		add_action( 'template_redirect', [ $this, 'process' ] );
	}

	public function init() {
		$this->register_shortcodes();
	}

	private function register_shortcodes() {
		add_shortcode( 'vote_thumb', [ $this, 'vote_thumb_shortcode' ] );
		add_shortcode( 'vote_thumb_gallery', [ $this, 'vote_thumb_gallery_shortcode' ] );
		add_shortcode( 'vote_list', [ $this, 'vote_list_shortcode' ] );
		add_shortcode( 'vote_message', [ $this, 'vote_message_shortcode' ] );
	}

	public function vote_thumb_shortcode() {
		$the_loai       = rwmb_meta( 'type' );

		if ( 'Hình ảnh' === $the_loai->name ) {
			$image_object = new Image();
			$images       = $image_object->get_images( get_the_ID() );

			if ( empty( $images ) ) {
				return '';
			}

			$image = reset( $images );
			return '<a class="wv-thumbnail-link" href="#' . esc_html( get_the_ID() ) . '"><img src="' . esc_url( $image['large'] ) . '"></a>';
		} else {
			$video_link     = rwmb_get_value( 'video' );
			$cut_video_link = get_youtube_video_ID( $video_link );
			return '<a class="wv-thumbnail-video-link" href="' . esc_html( $video_link ) . '"><img src="https://i.ytimg.com/vi/' . esc_html( $cut_video_link ) . '/maxresdefault.jpg"></a>';
		}
	}

	public function vote_thumb_gallery_shortcode() {
		$the_loai       = rwmb_meta( 'type' );

		$content = '<div id="' . esc_html( get_the_ID() ) . '" class="wv-gallery-thumbnail mfp-hide">';
		if ( 'Hình ảnh' === $the_loai->name ) {
			$image_object = new Image();
			$images       = $image_object->get_images( get_the_ID() );
			if ( empty( $images ) ) {
				return '';
			}

			foreach ( $images as $image ) {
				$content .= '<div class="slider-thumb-gallery"><img src="' . esc_url( $image['large'] ) . '"></div>';
			}

		}
		$content .= '</div>';
		return $content;
	}

	public function vote_list_shortcode() {
		$content = '<div class="wv-votes"><span class="wv-vote-number">' . intval( get_post_meta( get_the_ID(), 'votes', true ) ) . '</span> ' . __( 'lượt bình chọn', '2vn' ) . '</div>';

		// For webview in the chat bot, `_id` must set.
		$id = filter_input( INPUT_GET, '_id', FILTER_SANITIZE_STRING );
		if ( $id && 1 === $this->auth->authenticate( $id ) ) {
			return $content;
		}

		// After authentication in webview, or in normal browser, user must log in.
		if ( ! $this->auth->is_logged_in() ) {
			return $content;
		}

		$content .= '<a class="btn" href="' . add_query_arg( [ 'vote' => 'true', 'post_id' => get_the_ID() ] ) . '">' . __( 'Bình chọn', '2vn' ) . '</a>';
		return $content;
	}

	public function vote_message_shortcode() {
		if ( 'success' === filter_input( INPUT_GET, 'vote', FILTER_SANITIZE_STRING ) ) {
			return '<div class="wv-confirm">' . __( 'Bạn đã bình chọn thành công!', '2vn' ) . '</div>';
		}
		return '';
	}

	public function process() {
		if ( ! is_page( 'binh-chon-bai-du-thi-webview' ) && ! is_singular( 'bai-du-thi' ) ) {
			return;
		}

		if ( $this->can_vote() ) {
			$this->vote();
		}
	}

	private function can_vote() {
		$vote = filter_input( INPUT_GET, 'vote', FILTER_SANITIZE_STRING );
		if ( 'true' !== $vote ) {
			return false;
		}

		$id = filter_input( INPUT_GET, '_id', FILTER_SANITIZE_STRING );
		if ( $id && 1 === $this->auth->authenticate( $id ) ) {
			$message = '<p>' . esc_html__( 'Lỗi không xác thực được người dùng. Vui lòng liên hệ với quản trị viên.', '2vn' ) . '</p>';
			error( $message );
		}

		$user = $this->auth->get_user();
		if ( null === $user ) {
			$message = '<p>' . esc_html__( 'Vui lòng đăng nhập để bình chọn.', '2vn' ) . '</p>';
			error( $message );
		}
		$this->user_id = $this->auth->get_user_id();
		$this->user = $user;

		return true;
	}

	private function vote() {
		$points = $this->get_vote_points();
		if ( 1 > $points ) {
			$message = '<p>' . esc_html__( 'Bạn đã hết lượt bình chọn.', '2vn' ) . '</p>';
			error( $message );
		}

		if ( is_singular( 'bai-du-thi' ) ) {
			$post_id = get_the_ID();
		} else {
			$post_id = filter_input( INPUT_GET, 'post_id', FILTER_SANITIZE_NUMBER_INT );
		}
		if ( ! $post_id ) {
			$message = '<p>' . esc_html__( 'Bài dự thi không hợp lệ.', '2vn' ) . '</p>';
			error( $message );
		}

		$votes = (int) get_post_meta( $post_id, 'votes', true );
		$votes++;
		update_post_meta( $post_id, 'votes', $votes );

		$this->update_vote_points();

		$url = add_query_arg( 'vote', 'success' );
		wp_safe_redirect( $url );
		die;
	}

	private function get_vote_points() {
		$points = get( 'getVoteById', [
			'_id' => $this->user_id,
		] );

		if ( ! is_array( $points ) ) {
			return 0;
		}
		return $points[0]->PointVote;
	}

	private function update_vote_points() {
		post( 'executeVoteById', [
			'_id' => $this->user_id,
		] );
	}
}
