<?php
class Auth {
	const REGISTER_URL = 'https://m.me/2vietnam.vn';
	const LOGIN_URL    = 'https://m.me/2vietnam.vn/?ref=login:';

	public function __construct() {
		add_action( 'init', [ $this, 'init' ] );
		add_action( 'template_redirect', [ $this, 'process' ] );
		add_filter( 'wp_nav_menu_items', [ $this, 'add_account_links' ], 10, 2 );
	}

	public function init() {
		$this->register_shortcodes();
		$this->set_wp_user();
	}

	private function set_wp_user() {
		if ( is_admin() || ! is_user_logged_in() ) {
			return;
		}
		if ( session_status() === PHP_SESSION_NONE ) {
			session_start();
		}
		$user = wp_get_current_user();
		$this->set_user_id( $user->user_login );
	}

	public function process() {
		if ( session_status() === PHP_SESSION_NONE ) {
			session_start();
		}
		$this->login();
		$this->logout();
	}

	private function register_shortcodes() {
		add_shortcode( 'login', [ $this, 'login_shortcode' ] );
		add_shortcode( 'login_confirm', [ $this, 'login_confirm_shortcode' ] );
		add_shortcode( 'register', [ $this, 'register_shortcode' ] );
	}

	public function login_shortcode() {
		if ( $this->is_logged_in() ) {
			$user     = $this->get_user();
			$content  = '<p style="text-align: center">' . sprintf( __( 'Bạn đã đăng nhập qua Facebook với tên <strong>%s</strong>.', '2vn' ), $user->first_name ) . '</p>';
			$url      = add_query_arg( 'action', 'logout' );
			$content .= '<p style="text-align: center"><a href="' . $url . '" class="btn">' . __( 'Đăng xuất', '2vn' ) . '</a></p>';
			return $content;
		}
		$content = '<p style="text-align: center"><a href="' . self::LOGIN_URL . '" class="btn">' . esc_html__( 'Đăng nhập bằng Facebook', '2vn' ) . '</a></p>';

		if ( 'logout' === filter_input( INPUT_GET, 'action', FILTER_SANITIZE_STRING ) ) {
			$content = '<p style="text-align: center">' . __( 'Bạn đã đăng xuất thành công.', '2vn' ) . '</p>' . $content;
		}
		return $content;
	}

	public function login_confirm_shortcode() {
		$status = filter_input( INPUT_GET, 'status', FILTER_SANITIZE_STRING );

		if ( $this->is_logged_in() && null === $status ) {
			$user = $this->get_user();
			return '<p style="text-align: center">' . sprintf( __( 'Bạn đã đăng nhập qua Facebook với tên <strong>%s</strong>.', '2vn' ), $user->first_name ) . '</p>';
		}

		if ( null === $status ) {
			$url = add_query_arg( 'confirm', 'true' );
			?>
			<script>
				var count = 6;
				var url = "<?php echo $url ?>";
				var redirect = 'https://demo.2vietnam.vn' + url;
				function countDown() {
					var timer = document.getElementById("timer");
					if ( count > 0 ) {
						count--;
						timer.innerHTML = "<b>" + count + "</b>";
						setTimeout( 'countDown()', 1000 );
					} else {
						window.location.href = redirect;
					}
				}
			</script>
			<?php

			$content  = '<p style="text-align: center">' . __( 'Bạn đã xác nhận qua Facebook thành công. Xin vui lòng đợi hệ thống xử lý.', '2vn' ) . '</p>';
			$content .= '<p id="timer" style="text-align: center"><script>countDown();</script></p>';

		} elseif ( 1 == $status ) {
			$content = '<p style="text-align: center">' . __( 'Đăng nhập không thành công. Không thể tạo tài khoản cho bạn. Vui lòng liên hệ quản trị viên để được trợ giúp.', '2vn' ) . '</p>';
		} else {
			$user    = $this->get_user();
			$content = '<p style="text-align: center">' . sprintf( __( 'Bạn đã đăng nhập thành công qua Facebook với tên <strong>%s</strong>.', '2vn' ), $user->first_name ) . '</p>';
		}

		return $content;
	}

	public function register_shortcode() {
		if ( $this->is_logged_in() ) {
			return '<p>' . __( 'Bạn đã đăng nhập', '2vn' ) . '</p>';
		}
		return '<a href="' . self::REGISTER_URL . '" class="btn">' . esc_html__( 'Đăng ký bằng Facebook', '2vn' ) . '</a>';
	}

	private function login() {
		$id      = filter_input( INPUT_GET, '_id', FILTER_SANITIZE_STRING );
		$otp     = filter_input( INPUT_GET, 'otp', FILTER_SANITIZE_STRING );
		$confirm = filter_input( INPUT_GET, 'confirm', FILTER_SANITIZE_STRING );

		if ( ! $id || ! $otp || 'true' !== $confirm ) {
			return;
		}

		$data = post(
			'checkOtp',
			[
				'_id' => $id,
				'OTP' => $otp,
			]
		);

		$status = $this->authenticate( $data->Data->_id );

		if ( 2 === $status || 3 === $status ) {
			wp_safe_redirect( home_url() );
		} else {
			$url    = remove_query_arg( [ '_id', 'otp', 'confirm' ] );
			$url    = add_query_arg( 'status', $status, $url );

			wp_safe_redirect( $url );
		}
		die;
	}

	public function authenticate( $id ) {
		$status = $this->register( $id );
		if ( 1 !== $status ) {
			$this->set_user_id( $id );
		}
		return $status;
	}

	/**
	 * Register an user
	 *
	 * @param int $id User ID, used as login.
	 * @return int 1 = cannot register user, 2 = register succesffully, 3 = user exists.
	 */
	public function register( $id ) {
		if ( username_exists( $id ) ) {
			return 3;
		}
		$user_data = get( 'getMembersById', [ '_id' => $id ] );
		$user_data = reset( $user_data );
		$result    = wp_insert_user(
			[
				'user_login'   => $id,
				'user_pass'    => wp_generate_password(),
				'first_name'   => $user_data->Name,
			]
		);

		if ( is_wp_error( $result ) ) {
			return 1;
		}

		// Them user profile picture.
		// $profile_picture = $user_data->Info->profile_pic;
		// update_user_meta( $result, 'profile_pic_url', $profile_picture );

		// Gui email confirm.
		if ( function_exists( 'pll_current_language' ) ) {
			if ( 'English' === pll_current_language( 'name' ) ) {
				$language = 'english';
			} else {
				$language = 'tiengviet';
			}
		}
		$send_to = $user_data->Email;
		$name    = $user_data->Name;
		send_email_confirm( $send_to, $name, $language );

		return 2;
	}

	public function logout() {
		if ( 'logout' === filter_input( INPUT_GET, 'action', FILTER_SANITIZE_STRING ) ) {
			unset( $_SESSION['_id'] );
			wp_logout();
		}
	}

	public function is_logged_in() {
		return ! empty( $_SESSION['_id'] );
	}

	public function get_user_id() {
		return $_SESSION['_id'] ?? null;
	}

	public function set_user_id( $id ) {
		$_SESSION['_id'] = $id;
	}

	public function get_user() {
		$user_id = $this->get_user_id();
		$user    = $user_id ? get_user_by( 'login', $user_id ) : null;
		return ! empty( $user ) ? $user : null;
	}

	public function add_account_links( $items, $args ) {
		$user = $this->get_user();
		if ( ( empty( $user ) ) && ( 'Tiếng Việt' === pll_current_language( 'name' ) ) ) {
			$items .= '<li class="menu_button btn-primary-2"><a href="/dang-nhap/">' . __( 'Đăng nhập', '2vn' ) . '</a></li>';
			$items .= '<li class="menu_button btn-primary"><a href="/dang-ky/">' . __( 'Đăng kí', '2vn' ) . '</a></li>';
			return $items;
		}

		if ( ( empty( $user ) ) && ( 'English' === pll_current_language( 'name' ) ) ) {
			$items .= '<li class="menu_button btn-primary-2"><a href="/en/sign-in">' . __( 'Đăng nhập', '2vn' ) . '</a></li>';
			$items .= '<li class="menu_button btn-primary"><a href="/en/register">' . __( 'Đăng kí', '2vn' ) . '</a></li>';
			return $items;
		}

		if ( 'Tiếng Việt' === pll_current_language( 'name' ) ) {
			$items .= '<li class="my-account"><div class="my-account__hi">' . sprintf( __( 'Xin chào! <strong><a href="%s">%s</a></strong>', '2vn' ), home_url( '/dang-ky-thong-tin/' ), $user->first_name ) . '</div><div class="my-account__link"><a href="' . add_query_arg( 'action', 'logout' ) . '">' . __( 'Đăng xuất', '2vn' ) . '</a></li>';
		}

		if ( 'English' === pll_current_language( 'name' ) ) {
			$items .= '<li class="my-account"><div class="my-account__hi">' . sprintf( __( 'Hi! <strong><a href="%s">%s</a></strong>', '2vn' ), home_url( '/en/dang-ky-thong-tin-2/' ), $user->first_name ) . '</div><div class="my-account__link"><a href="' . add_query_arg( 'action', 'logout' ) . '">' . __( 'Logout', '2vn' ) . '</a></li>';
		}

		$id = $user->user_login;
		$user_data = get( 'getMembersById', [ '_id' => $id ] );
		$user_data = reset( $user_data );
		$profile_pic = $user_data->Info->profile_pic;
		if ( ! empty( $profile_pic ) ) {
			$items .= '<img style="width: 40px;" src="' . $profile_pic . '">';
		}

		if ( ! empty( $user->google_profile_image ) ) {
			$items .= '<img style="width: 40px;" src="' . $user->google_profile_image . '">';
		}

		return $items;
	}
}
