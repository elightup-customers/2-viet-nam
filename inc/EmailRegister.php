<?php
class EmailRegister {
	private $auth;

	public function __construct( $auth ) {
		$this->auth = $auth;

		add_filter( 'rwmb_profile_register_fields', [ $this, 'change_register_fields' ] );
		add_action( 'rwmb_profile_after_create_user', [$this, 'send_user_data'] );
		add_action( 'rwmb_profile_after_create_user', [$this, 'add_user_meta_key'] );

		add_action( 'wp_login', [$this, 'set_session_id'], 10, 2 );
		add_action( 'template_redirect', [ $this, 'check_active_user' ] );
	}

	public function change_register_fields( $fields ) {
		unset( $fields['username'] );
		return $fields;
	}

	public function send_user_data( $user_object ) {
		if ( empty( $user_object->user_id ) ) {
			return;
		}
		$user = get_userdata( $user_object->user_id );
		$data = [
			'Name'       => '',
			'Type'       => 'Member',
			'OriginId'   => $user_object->user_id,
			'Origin'     => 'Email',
			'Language'   => 'VN',
			'Email'      => $user->user_email,
		];
		$result = post( 'saveMembersEmail', $data );

		// Update username to `_id` from chatbot.
		change_username( $user->user_login, $result->Data->_id );

		if ( function_exists( 'pll_current_language' ) ) {
			if ( 'English' === pll_current_language( 'name' ) ) {
				$language = 'english';
			} else {
				$language = 'tiengviet';
			}
		}
		$send_to      = $user->user_email;
		$name         = $user->user_email;
		$key          = md5( $user_object->user_id . 'checkxacthuc' );
		$link_confirm = 'https://demo.2vietnam.vn?active_user=' . $user_object->user_id . '&key=' . $key;
		send_email_confirm( $send_to, $name, $language, $link_confirm );
	}

	public function set_session_id( $user_login, $user_object ) {
		$active = get_user_meta( $user_object->ID, 'inactive' );
		if ( ! empty( $active ) ) {
			wp_logout();
			wp_die( 'Account của bạn chưa được xác thực' );
		} else {
			$this->auth->set_user_id( $user_login );
		}
	}

	public function add_user_meta_key( $user_object ){
		if ( empty( $user_object->user_id ) ) {
			return;
		}
		add_user_meta( $user_object->user_id, 'inactive', 1 );
	}

	public function check_active_user(){
		$user_id = $_GET['active_user'];
		$key     = $_GET['key'];

		if ( isset( $_GET['active_user'] ) ) {
			delete_user_meta( $_GET['active_user'], 'inactive' );
		}

		if ( $key == md5( $user_id . 'checkxacthuc' ) ) {
			$home_url = home_url();
			wp_die( 'Xác thực thành công. Vui lòng click vào <a href="' . $home_url . '">đây</a> để trở về trang chủ.' );
		}

	}
}
