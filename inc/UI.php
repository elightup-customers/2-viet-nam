<?php
class UI {
	public function __construct() {
		add_action( 'add_meta_boxes', [ $this, 'remove_meta_boxes' ], 99 );
		add_action('after_setup_theme', [ $this, 'remove_admin_bar'] );
	}

	public function remove_meta_boxes() {
		remove_meta_box( 'members-cp', null, 'advanced' );
	}

	public function remove_admin_bar() {
		if ( ! current_user_can( 'manage_options' ) && ! is_admin() ) {
			show_admin_bar( false );
		}
	}
}
