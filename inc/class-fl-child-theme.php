<?php

/**
 * Helper class for child theme functions.
 *
 * @class FLChildTheme
 */
final class FLChildTheme {

	/**
	 * Enqueues scripts and styles.
	 *
	 * @return void
	 */
	public static function enqueue_scripts() {
		wp_enqueue_style( 'fl-child-theme', FL_CHILD_THEME_URL . '/style.css' );
		// wp_enqueue_style( 'fl-child-theme-recent-posts', FL_CHILD_THEME_URL . '/css/widget/recent-posts.css' );
		wp_enqueue_style( 'fl-child-theme-slick', FL_CHILD_THEME_URL . '/css/slick.css' );

		wp_enqueue_script( 'fl_script', get_stylesheet_directory_uri() . '/js/script.js', array( 'jquery' ), '', true );
		wp_enqueue_script( 'fl_slick', get_stylesheet_directory_uri() . '/js/slick.js', array( 'jquery' ), '', true );
	}
}
