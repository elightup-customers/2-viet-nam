<?php
/*
Template Name: Long form
Template Post Type: post
*/

get_header( 'long-form' ); ?>

<div class="container">
	<div class="row">

		<div class="fl-content">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					get_template_part( 'content', 'single-long-form' );
				endwhile;
			endif; ?>
		</div>

	</div>
</div>

<?php get_footer(); ?>
