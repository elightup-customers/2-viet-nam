<?php

$show_thumbs = FLTheme::get_setting( 'fl-posts-show-thumbs' );
$thumb_size  = FLTheme::get_setting( 'fl-posts-thumb-size' );
?>
<?php do_action( 'fl_before_post' ); ?>
<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" itemscope itemtype="https://schema.org/BlogPosting">

	<?php if ( has_post_thumbnail() ) : ?>
		<h1 class="fl-post-thumb">
			<?php
			the_post_thumbnail( 'full', array(
				'itemprop' => 'image',
			) ); ?>
		</h1>
	<?php endif; ?>

	<header class="fl-post-header">

	</header><!-- .fl-post-header -->


	<?php do_action( 'fl_before_post_content' ); ?>

	<div class="fl-post-content clearfix" itemprop="text">
		<?php

		the_content();

		wp_link_pages( array(
			'before'         => '<div class="fl-post-page-nav">' . _x( 'Pages:', 'Text before page links on paginated post.', 'fl-automator' ),
			'after'          => '</div>',
			'next_or_number' => 'number',
		) );

		?>
	</div><!-- .fl-post-content -->

	<?php if ( has_post_thumbnail() && 'beside' == $show_thumbs ) : ?>
		</div>
	</div>
	<?php endif; ?>

	<?php FLTheme::post_navigation(); ?>
	<?php do_action( 'fl_after_post_content' ); ?>

</article>
<div id="fb-comments-popup" class="white-popup mfp-hide"><?php flchild_comments(); ?></div>
<div href="#fb-comments-popup" class="open-popup-link full-width" class="">
	<span class="fb-comments-count" data-href="<?php the_permalink(); ?>"></span>
	<a>Bình luận</a>
</div>

<?php do_action( 'fl_after_post' ); ?>

<!-- .fl-post -->
