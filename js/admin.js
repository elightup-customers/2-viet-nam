jQuery( function ( $ ) {
	'use strict';

	function sendEmail() {
		$( '#btn-sendemail' ).on( 'click', function() {
			var data = {
				action : 'send_email',
				post_id: ajax_object.post_id,
				_wpnonce: ajax_object.nonce
			};
			$.post(ajax_object.ajax_url, data).done( function( response ) {
				if ( response.success ) {
					var $text = '<span style="padding: .75em 1.25em; border: 1px solid #d0e9c6; border-radius: .25em; background: #dff0d8; color: #3c763d; display: block;">Bạn đã gửi thành công</span>';
					$( '.btn-sendemail #btn-sendemail' ).hide();
					$( '.btn-sendemail' ).append( $text );
				}
			} );
		} );


		$( '#btn-sendemail-kohople' ).on( 'click', function() {
			var data = {
				action : 'send_email_kohople',
				post_id: ajax_object.post_id,
				_wpnonce: ajax_object.nonce_kohople
			};
			$.post(ajax_object.ajax_url, data).done( function( response ) {
				if ( response.success ) {
					var $text = '<span style="padding: .75em 1.25em; border: 1px solid #d0e9c6; border-radius: .25em; background: #dff0d8; color: #3c763d; display: block;">Bạn đã gửi thành công</span>';
					$( '.btn-sendemail-kohople #btn-sendemail-kohople' ).hide();
					$( '.btn-sendemail-kohople' ).append( $text );
				}
			} );
		} );

		$( '#btn-sendemail-binhchonthang' ).on( 'click', function() {
			var data = {
				action : 'send_email_binhchonthang',
				post_id: ajax_object.post_id,
				_wpnonce: ajax_object.nonce_binhchonthang
			};
			$.post(ajax_object.ajax_url, data).done( function( response ) {
				if ( response.success ) {
					var $text = '<span style="padding: .75em 1.25em; border: 1px solid #d0e9c6; border-radius: .25em; background: #dff0d8; color: #3c763d; display: block;">Bạn đã gửi thành công</span>';
					$( '.btn-sendemail-binhchonthang #btn-sendemail-binhchonthang' ).hide();
					$( '.btn-sendemail-binhchonthang' ).append( $text );
				}
			} );
		} );

		$( '#btn-sendemail-vongtrong' ).on( 'click', function() {
			var data = {
				action : 'send_email_vongtrong',
				post_id: ajax_object.post_id,
				_wpnonce: ajax_object.nonce_vongtrong
			};
			$.post(ajax_object.ajax_url, data).done( function( response ) {
				if ( response.success ) {
					var $text = '<span style="padding: .75em 1.25em; border: 1px solid #d0e9c6; border-radius: .25em; background: #dff0d8; color: #3c763d; display: block;">Bạn đã gửi thành công</span>';
					$( '.btn-sendemail-vongtrong #btn-sendemail-vongtrong' ).hide();
					$( '.btn-sendemail-vongtrong' ).append( $text );
				}
			} );
		} );

		$( '#btn-sendemail-workshop' ).on( 'click', function() {
			var data = {
				action : 'send_email_workshop',
				post_id: ajax_object.post_id,
				_wpnonce: ajax_object.nonce_workshop
			};
			$.post(ajax_object.ajax_url, data).done( function( response ) {
				if ( response.success ) {
					var $text = '<span style="padding: .75em 1.25em; border: 1px solid #d0e9c6; border-radius: .25em; background: #dff0d8; color: #3c763d; display: block;">Bạn đã gửi thành công</span>';
					$( '.btn-sendemail-workshop #btn-sendemail-workshop' ).hide();
					$( '.btn-sendemail-workshop' ).append( $text );
				}
			} );
		} );

		$( '#btn-sendemail-gala' ).on( 'click', function() {
			var data = {
				action : 'send_email_gala',
				post_id: ajax_object.post_id,
				_wpnonce: ajax_object.nonce_gala
			};
			$.post(ajax_object.ajax_url, data).done( function( response ) {
				if ( response.success ) {
					var $text = '<span style="padding: .75em 1.25em; border: 1px solid #d0e9c6; border-radius: .25em; background: #dff0d8; color: #3c763d; display: block;">Bạn đã gửi thành công</span>';
					$( '.btn-sendemail-gala #btn-sendemail-gala' ).hide();
					$( '.btn-sendemail-gala' ).append( $text );
				}
			} );
		} );
	}

	sendEmail();
} );