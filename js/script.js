jQuery( function ( $ ) {
	'use strict';

	var clickEvent = 'ontouchstart' in window ? 'touchstart' : 'click';
	var $body = $( 'body' );
	var isRTL = $body.hasClass( 'rtl' );
	var $marquee = $( '.js-trending-slide' );
	var speed = parseInt( $marquee.attr( 'data-speed' ) );
	var $current;

	function initRotateTrending() {
		$( '.js-trending-rotate' ).slick( {
			autoplay      : true,
			prevArrow     : '',
			nextArrow     : '<button class="slick-next"><i class="icofont icofont-arrow-right"></i></button>',
			adaptiveHeight: true
		} );
	}

	var handleTrendingAnimation = function ( currentMargin ) {
		currentMargin = currentMargin || 0;
		$current = $( this );

		var firstItemWidth = $current.outerWidth();
		if ( isRTL ) {
			$current.animate( {
				'margin-right': - firstItemWidth
			}, speed * (
				firstItemWidth + currentMargin
			), 'linear', function () {
				var $cloneItem = $current.clone();
				$cloneItem.css( 'margin-right', '0' );
				$marquee.append( $cloneItem );

				$current.remove();
				handleTrendingAnimation.call( $marquee.children().eq( 0 ) ); // set new first item, not $current, $current is remove();
			} );
		} else {
			$current.animate( {
				'margin-left': - firstItemWidth
			}, speed * (
				firstItemWidth + currentMargin
			), 'linear', function () {
				var $cloneItem = $current.clone();
				$cloneItem.css( 'margin-left', '0' );
				$marquee.append( $cloneItem );

				$current.remove();
				handleTrendingAnimation.call( $marquee.children().eq( 0 ) ); // set new first item, not $current, $current is remove();
			} );
		}
	};

	var initTrendingSlide = function () {
		handleTrendingAnimation.call( $marquee.children().eq( 0 ) );

		$marquee.hover( function () {
			$current.clearQueue().stop();
		}, function () {
			if ( isRTL ) {
				var currentMargin = $current.css( 'margin-right' ).replace( 'px', '' );
			} else {
				var currentMargin = $current.css( 'margin-left' ).replace( 'px', '' );
			}
			handleTrendingAnimation.call( $current, parseInt( currentMargin ) );
		} );
	};


	function slickSlider() {
		$('.gallery-thumbnail').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
			arrows: false,
			asNavFor: '.slider-gallery-thumbnail',
			adaptiveHeight: true,
		}); // slick
		$('.slider-gallery-thumbnail').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: true,
			asNavFor: '.gallery-thumbnail',
			focusOnSelect: true,
			dots: false,
			arrows: false,
			// centerMode: true,
			// centerPadding: '0',
			// focusOnSelect: true
		});
	}

	/**
	 * Counter Statistic.
	 */
	function countStatistic(argument) {
		var a = 0;
		$(window).scroll(function() {
			var oTop = $('.statistics-content').offset().top - window.innerHeight;
			if (a == 0 && $(window).scrollTop() > oTop) {
				$('.number').each(function() {
					var $this = $(this),
						countTo = $this.attr('data-count');
					$({ countNum: 0 }).animate({ countNum: countTo }, {
						duration: 2000,
						easing: 'swing',
						step: function() {
							$this.text(Math.ceil(this.countNum));
						},
					});
				});
				a = 1;
			}

		});
	}

	function popupListBaiduthi() {
		$( '.wv-thumbnail-link' ).click(function(){
			var items = [];

			$( $(this).attr('href') ).find('.slider-thumb-gallery').each(function() {
				items.push( {
				  src: $(this)
				} );
			});

			$.magnificPopup.open({
				items:items,
				closeOnContentClick: true,
				gallery: {
				  enabled: true
				}
			});
		});
		$( '.wv-thumbnail-video-link' ).magnificPopup({
			type: 'iframe',
			preloader: false,
			mainClass: 'mfp-fade',
			removalDelay: 160,
			fixedContentPos: false
		});
	}

	function popupCommentSingleLongform() {
		$( '.open-popup-link' ).magnificPopup({
			type: 'inline',
		});
	}

	function popupThele() {
		$( '.open-popup' ).magnificPopup({
			type: 'inline',
		});
	}

	function changeText() {
		var $radio = $( '.rwmb-input-list label' );
		$radio.contents()[1].textContent='Images';
	}

	function changeText2() {
		var $text = $( 'li[data-filter=".uabb-masonary-cat-6"]' );
		$text.text( 'Images' );
	}

	initTrendingSlide();
	handleTrendingAnimation();
	initRotateTrending();
	slickSlider();
	if ( $("body").hasClass("home") ) {
		countStatistic();
	}
	popupListBaiduthi();
	if ( $("body").hasClass("post-template-single-long-form") ) {
		popupCommentSingleLongform();
	}
	popupThele();
	if ( $("body").hasClass("page-id-1325") ) {
		changeText();
	}

	if ( $("body").hasClass("archive-bai-du-thi-en") ) {
		changeText2();
	}
} );